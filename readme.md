# xmsh - a mesh data document format

| Gitlab | Status | Github| Status |
| ---             |    ---     | ---             |    ---     |
| `master` |  [![GitLab Build Status](https://gitlab.com/libxmsh/xmsh/badges/master/build.svg)](https://gitlab.com/libxmsh/xmsh/pipelines)	| `master` |[![Build Status](https://travis-ci.org/libxmsh/xmsh.svg?branch=master)](https://travis-ci.org/libxmsh/xmsh) 	|
| `develop` | [![GitLab Build Status](https://gitlab.com/libxmsh/xmsh/badges/develop/build.svg)](https://gitlab.com/libxmsh/xmsh/pipelines) | `develop`|[![Build Status](https://travis-ci.org/libxmsh/xmsh.svg?branch=develop)](https://travis-ci.org/libxmsh/xmsh)	|

--------------------------------------------------------------------------------

## What is xmsh?

xmsh is a "dumb" mesh data document format. It stores the mesh and
stores data attached to each element of the mesh, but does not interepret that
data in any manner. The interpretation is left to the user instead. This allows
the file format to be extremely versatile and general. This is a C++ reference
implementation for the same.


## For Users
- [Installation Instructions](doc/web/install.md)
- [Reference Manual](doc/tex/book/book.pdf)

## For Developers
- [Instruction for Developers](doc/web/developer.md)


# License

xmsh is distributed under the terms of both the MIT License and the Apache
License (version 2.0). User may choose either license, at their option. All
contributions must be made under both the MIT and the Apache-2.0 license.

Copyrights and patents in the xmsh project are retained by contributors.
No copyright assignment is required to contribute to xmsh.
