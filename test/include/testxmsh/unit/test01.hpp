#ifndef TESTXMSH_UNIT_TEST01_HPP
#define TESTXMSH_UNIT_TEST01_HPP

#include <cmath>
#include <xmsh/xmsh_data.hpp>

namespace testxmsh::unit {
auto generate_xmshdata_01() -> xmsh::xmsh_data<std::allocator<void>>;

} // namespace testxmsh::unit
#endif // TESTXMSH_UNIT_TEST01_HPP
