#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_V1_GRID_DATA_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_V1_GRID_DATA_HPP

#include <xmsh/error_code.hpp>
#include <xmsh/grid_data.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_grid_data_valid01()
  -> xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>;
auto generate_grid_data_output_valid01()
  -> xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>;
} // namespace testxmsh::unit::detail::streamio::v1

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_V1_GRID_DATA_HPP
