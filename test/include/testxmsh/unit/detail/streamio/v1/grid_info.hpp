#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_V1_GRID_INFO_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_V1_GRID_INFO_HPP

#include <xmsh/grid_info.hpp>

namespace testxmsh::unit::detail::streamio {
auto generate_grid_info_valid01() -> xmsh::grid_info<std::allocator<void>>;
auto generate_grid_info_output_valid01() -> xmsh::grid_info<std::allocator<void>>;
} // namespace testxmsh::unit::detail::streamio

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_V1_GRID_INFO_HPP
