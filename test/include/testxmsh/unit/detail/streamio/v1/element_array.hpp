#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_V1_ELEMENT_ARRAY_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_V1_ELEMENT_ARRAY_HPP

#include <xmsh/element_array.hpp>
#include <xmsh/error_code.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_element_array_valid01()
  -> xmsh::element_array<std::allocator<void>>;
auto generate_element_array_output_valid01()
  -> xmsh::element_array<std::allocator<void>>;
} // namespace testxmsh::unit::detail::streamio::v1

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_V1_ELEMENT_ARRAY_HPP
