#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_V1_VERTEX_ARRAY_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_V1_VERTEX_ARRAY_HPP

#include <xmsh/error_code.hpp>
#include <xmsh/vertex_array.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_vertex_array_valid01() -> xmsh::vertex_array<std::allocator<void>>;
auto generate_vertex_array_output_valid01()
  -> xmsh::vertex_array<std::allocator<void>>;
} // namespace testxmsh::unit::detail::streamio::v1

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_V1_VERTEX_ARRAY_HPP
