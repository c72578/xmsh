#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_V1_PREAMBLE_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_V1_PREAMBLE_HPP

#include <xmsh/detail/preamble.hpp>
#include <xmsh/error_code.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_preamble_valid01()
  -> xmsh::detail::preamble<xmsh::file_type::ascii, std::allocator<void>>;
auto generate_preamble_output_valid01()
  -> xmsh::detail::preamble<xmsh::file_type::ascii, std::allocator<void>>;
} // namespace testxmsh::unit::detail::streamio::v1

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_V1_PREAMBLE_HPP
