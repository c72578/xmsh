#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_V1_FILE_ARRAY_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_V1_FILE_ARRAY_HPP

#include <xmsh/file_array.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_filearray_valid01() -> xmsh::file_array<std::allocator<void>>;
auto generate_filearray_output_valid01()
  -> xmsh::file_array<std::allocator<void>>;
} // namespace testxmsh::unit::detail::streamio::v1

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_V1_FILE_ARRAY_HPP
