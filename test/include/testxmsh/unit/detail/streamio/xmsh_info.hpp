#ifndef TESTXMSH_UNIT_DETAIL_STREAMIO_XMSH_INFO_HPP
#define TESTXMSH_UNIT_DETAIL_STREAMIO_XMSH_INFO_HPP

#include <xmsh/xmsh_info.hpp>

namespace testxmsh::unit::detail::streamio {
auto generate_xmsh_info_valid01() -> xmsh::xmsh_info;
auto generate_xmsh_info_output_valid01() -> xmsh::xmsh_info;
} // namespace testxmsh::unit::detail::streamio

#endif // TESTXMSH_UNIT_DETAIL_STREAMIO_XMSH_INFO_HPP
