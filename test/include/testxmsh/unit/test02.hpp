#ifndef TESTXMSH_UNIT_TEST02_HPP
#define TESTXMSH_UNIT_TEST02_HPP

#include <cmath>
#include <xmsh/xmsh_data.hpp>

namespace testxmsh::unit {
auto generate_xmshdata_02() -> xmsh::xmsh_data<std::allocator<void>>;

} // namespace testxmsh::unit
#endif // TESTXMSH_UNIT_TEST02_HPP
