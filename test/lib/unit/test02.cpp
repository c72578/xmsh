#include <testxmsh/unit/test02.hpp>

namespace testxmsh::unit {
auto generate_xmshdata_02() -> xmsh::xmsh_data<std::allocator<void>>
{
  xmsh::xmsh_data<std::allocator<void>> xmsh;

  xmsh.grid_info.label = "valid02";
  xmsh.grid_info.wdim = xmsh::world_dimension::one;
  xmsh.grid_info.dim = xmsh::grid_dimension::one;

  /// Generate vertex_array
  {
    xmsh::vertex_array<std::allocator<void>> va;
    va.wdim = 1;
    auto& data = va.data;
    data.resize(129);
    for (std::size_t i = 0; i < 129; ++i) {
      data[i].index = i;
      auto const x = -1 + 2.0 / 128 * static_cast<double>(i);
      data[i].coord = std::vector{x};
    }
    xmsh.vertex_array = std::move(va);
  }

  /// Generate element_array
  {
    xmsh::element_array<std::allocator<void>> element_array;

    element_array.codim = 0;

    auto& data = element_array.data;
    data.resize(128);
    for (std::size_t i = 0; i < 128; ++i) {
      data[i].index = i;
      data[i].type = xmsh::element_type::edge;
      data[i].vertex_array.resize(2);
      data[i].vertex_array[0] = i;
      data[i].vertex_array[1] = i + 1;
    }

    xmsh.codim_element_array.resize(1);
    xmsh.codim_element_array[0] = std::move(element_array);
  }

  /// Generate Floating Point Data Array
  {
    auto gd = xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>();
    gd.codim = 0;
    gd.label = "density";
    gd.data.resize(128);
    for (std::size_t i = 0; i < 128; ++i) {
      gd.data[i].index = i;
      auto const x1 = ((static_cast<double>(i) - 64.0) / 128.0) * 2;
      auto const x2 = ((static_cast<double>(i) + 1 - 64.0) / 128.0) * 2;
      auto const x = 0.5 * (x1 + x2);
      gd.data[i].value = std::vector{std::sin(M_PI * x)};
    }
    xmsh.float_grid_data.resize(2);
    xmsh.float_grid_data[0].insert(std::make_pair("density", std::move(gd)));
  }

  /// Generate Integer Data Array
  {
    auto gd = xmsh::grid_data<xmsh::data_type::integer, std::allocator<void>>();
    gd.codim = 0;
    gd.label = "boundary_pair";
    gd.data.resize(128);
    for (std::size_t i = 0; i < 128; ++i) {
      gd.data[i].index = i;
      gd.data[i].value.resize(1);
      gd.data[i].value[0] = -1;
    }
    gd.data[0].value[0] = 127;
    gd.data[127].value[0] = 0;
    xmsh.integer_grid_data.resize(2);
    xmsh.integer_grid_data[0].insert(
      std::make_pair("boundary_pair", std::move(gd)));
  }

  return xmsh;
}

} // namespace testxmsh::unit
