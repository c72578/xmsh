#include <testxmsh/unit/detail/streamio/v1/vertex_array.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_vertex_array_valid01() -> xmsh::vertex_array<std::allocator<void>>
{
  xmsh::vertex_array<std::allocator<void>> va;
  va.wdim = 1;
  auto& data = va.data;
  data.resize(129);
  for (std::size_t i = 0; i < 129; ++i) {
    data[i].index = i;
    auto const x = -1 + 2.0 / 128 * static_cast<double>(i);
    data[i].coord = std::vector{x};
  }
  return va;
}

auto generate_vertex_array_output_valid01()
  -> xmsh::vertex_array<std::allocator<void>>
{
  xmsh::vertex_array<std::allocator<void>> va;
  va.wdim = 1;
  auto& data = va.data;
  data.resize(129);
  for (std::size_t i = 0; i < 129; ++i) {
    data[i].index = i;
    auto const x = -1 + 2.0 / 128 * static_cast<double>(i);
    data[i].coord = std::vector{x};
  }
  return va;
}
} // namespace testxmsh::unit::detail::streamio::v1
