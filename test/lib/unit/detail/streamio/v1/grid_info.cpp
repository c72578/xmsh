#include <testxmsh/unit/detail/streamio/v1/grid_info.hpp>

namespace testxmsh::unit::detail::streamio {
auto generate_grid_info_valid01() -> xmsh::grid_info<std::allocator<void>>
{
  xmsh::grid_info<std::allocator<void>> grid_info;

  grid_info.label = "valid01";
  grid_info.wdim = xmsh::world_dimension::one;
  grid_info.dim = xmsh::grid_dimension::one;

  return grid_info;
}

auto generate_grid_info_output_valid01() -> xmsh::grid_info<std::allocator<void>>
{
  xmsh::grid_info<std::allocator<void>> grid_info;

  grid_info.label = "output_valid01";
  grid_info.wdim = xmsh::world_dimension::one;
  grid_info.dim = xmsh::grid_dimension::one;

  return grid_info;
}
} // namespace testxmsh::unit::detail::streamio
