#include <testxmsh/unit/detail/streamio/v1/file_array.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_filearray_valid01() -> xmsh::file_array<std::allocator<void>>
{
  xmsh::file_array<std::allocator<void>> filearray;
  filearray.data.resize(3);
  filearray.data[0].index = 0;
  filearray.data[1].index = 1;
  filearray.data[2].index = 2;

  filearray.data[0].file_type = xmsh::file_type::ascii;
  filearray.data[1].file_type = xmsh::file_type::ascii;
  filearray.data[2].file_type = xmsh::file_type::binary;

  filearray.data[0].filename = "valid01.00.xmsp";
  filearray.data[1].filename = "valid01.01.xmsp";
  filearray.data[2].filename = "valid01.02.xmsb";
  return filearray;
}

auto generate_filearray_output_valid01()
  -> xmsh::file_array<std::allocator<void>>
{
  xmsh::file_array<std::allocator<void>> filearray;
  filearray.data.resize(3);
  filearray.data[0].index = 0;
  filearray.data[1].index = 1;
  filearray.data[2].index = 2;

  filearray.data[0].file_type = xmsh::file_type::ascii;
  filearray.data[1].file_type = xmsh::file_type::ascii;
  filearray.data[2].file_type = xmsh::file_type::binary;

  filearray.data[0].filename = "valid01.00.xmsp";
  filearray.data[1].filename = "valid01.01.xmsp";
  filearray.data[2].filename = "valid01.02.xmsb";
  return filearray;
}
} // namespace testxmsh::unit::detail::streamio::v1
