#include <testxmsh/unit/detail/streamio/v1/element_array.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_element_array_valid01()
  -> xmsh::element_array<std::allocator<void>>
{
  xmsh::element_array<std::allocator<void>> element_array;

  element_array.codim = 0;

  auto& data = element_array.data;
  data.resize(128);
  for (std::size_t i = 0; i < 128; ++i) {
    data[i].index = i;
    data[i].type = xmsh::element_type::edge;
    data[i].vertex_array.resize(2);
    data[i].vertex_array[0] = i;
    data[i].vertex_array[1] = i + 1;
  }

  return element_array;
}

auto generate_element_array_output_valid01()
  -> xmsh::element_array<std::allocator<void>>
{
  xmsh::element_array<std::allocator<void>> element_array;

  element_array.codim = 1;

  auto& data = element_array.data;
  data.resize(128);
  for (std::size_t i = 0; i < 128; ++i) {
    data[i].index = i;
    data[i].type = xmsh::element_type::edge;
    data[i].vertex_array.resize(2);
    data[i].vertex_array[0] = i;
    data[i].vertex_array[1] = i + 1;
  }

  return element_array;
}
} // namespace testxmsh::unit::detail::streamio::v1
