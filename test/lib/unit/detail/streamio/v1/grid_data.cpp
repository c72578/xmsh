#include <cmath>
#include <testxmsh/unit/detail/streamio/v1/grid_data.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_grid_data_valid01()
  -> xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>
{
  auto gd = xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>();
  gd.codim = 0;
  gd.label = "density";
  gd.data.resize(128);
  for (std::size_t i = 0; i < 128; ++i) {
    gd.data[i].index = i;
    auto const x1 = ((static_cast<double>(i) - 64.0) / 128.0) * 2;
    auto const x2 = ((static_cast<double>(i) + 1 - 64.0) / 128.0) * 2;
    auto const x = 0.5 * (x1 + x2);
    gd.data[i].value = std::vector{std::sin(M_PI * x)};
  }
  return gd;
}
auto generate_grid_data_output_valid01()
  -> xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>
{
  auto gd = xmsh::grid_data<xmsh::data_type::fp, std::allocator<void>>();
  gd.codim = 0;
  gd.label = "density";
  gd.data.resize(128);
  for (std::size_t i = 0; i < 128; ++i) {
    gd.data[i].index = i;
    auto const x1 = ((static_cast<double>(i) - 64.0) / 128.0) * 2;
    auto const x2 = ((static_cast<double>(i) + 1 - 64.0) / 128.0) * 2;
    auto const x = 0.5 * (x1 + x2);
    gd.data[i].value = std::vector{std::sin(M_PI * x)};
  }
  return gd;
}
} // namespace testxmsh::unit::detail::streamio::v1
