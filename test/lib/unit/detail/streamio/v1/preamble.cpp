#include <testxmsh/unit/detail/streamio/v1/preamble.hpp>

namespace testxmsh::unit::detail::streamio::v1 {
auto generate_preamble_valid01()
  -> xmsh::detail::preamble<xmsh::file_type::ascii, std::allocator<void>>
{
  constexpr auto ft = xmsh::file_type::ascii;
  xmsh::detail::preamble<ft, std::allocator<void>> preamble;
  preamble.data.push_back(
    xmsh::detail::SectionInfo<ft>{10, xmsh::section_type::vertexarray});
  preamble.data.push_back(
    xmsh::detail::SectionInfo<ft>{100, xmsh::section_type::griddata});
  return preamble;
}

auto generate_preamble_output_valid01()
  -> xmsh::detail::preamble<xmsh::file_type::ascii, std::allocator<void>>
{
  constexpr auto ft = xmsh::file_type::ascii;
  xmsh::detail::preamble<ft, std::allocator<void>> preamble;
  preamble.data.push_back(
    xmsh::detail::SectionInfo<ft>{10, xmsh::section_type::vertexarray});
  preamble.data.push_back(
    xmsh::detail::SectionInfo<ft>{100, xmsh::section_type::griddata});
  return preamble;
}
} // namespace testxmsh::unit::detail::streamio::v1
