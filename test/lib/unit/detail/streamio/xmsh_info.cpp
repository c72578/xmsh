#include <testxmsh/unit/detail/streamio/xmsh_info.hpp>

namespace testxmsh::unit::detail::streamio {
auto generate_xmsh_info_valid01() -> xmsh::xmsh_info
{
  xmsh::xmsh_info xmsh_info;

  xmsh_info.version = xmsh::Version::one;
  xmsh_info.is_multifile = xmsh::multifile::no;

  return xmsh_info;
}

auto generate_xmsh_info_output_valid01() -> xmsh::xmsh_info
{
  xmsh::xmsh_info xmsh_info;

  xmsh_info.version = xmsh::Version::one;
  xmsh_info.is_multifile = xmsh::multifile::no;

  return xmsh_info;
}
} // namespace testxmsh::unit::detail::streamio
