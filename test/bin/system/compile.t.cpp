#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <testxmsh/example.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/log_level.hpp>
#include <xmsh/meta/buildinfo.hpp>

TEST_CASE("Compilation Test", "[constructor]")
{
  constexpr auto buildinfo = xmsh::meta::buildinfo_t();
  REQUIRE(buildinfo.xmsh_info.major_version == 0);
  auto loglevel = xmsh::log_level::normal;
  auto const buildstring = xmsh::to_string_view(loglevel);
  REQUIRE(buildstring == "normal");
}
