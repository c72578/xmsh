#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <experimental/filesystem>
#include <fstream>
#include <testxmsh/meta/testpath.hpp>
#include <testxmsh/unit/test01.hpp>
#include <testxmsh/unit/test02.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/xmsh.hpp>

namespace fs = std::experimental::filesystem;
using A = std::allocator<void>;

constexpr auto const reldir = "/unit/";

TEST_CASE("XMSH Test01", "[xmsh]")
{
  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testdatadir = testpath.data;
  auto xmsh_info_datadir = std::string(testdatadir) + std::string(reldir);
  auto const validinput01 = xmsh_info_datadir + "test01/validinput01.xmsh";
  auto const xmshdata = xmsh::read_xmsh<A>(validinput01);
  auto const exact_xmshdata = testxmsh::unit::generate_xmshdata_01();
  REQUIRE(xmshdata == exact_xmshdata);
}

TEST_CASE("XMSH Test02", "[xmsh]")
{
  auto const exact_xmsh = testxmsh::unit::generate_xmshdata_02();

  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testoutputdir = testpath.output;
  auto xmsh_info_outputdir = std::string(testoutputdir) + std::string(reldir);

  xmsh::xmsh_info xmsh_info;
  xmsh_info.is_multifile = xmsh::multifile::no;
  xmsh_info.version = xmsh::Version::one;
  auto const outfilename = xmsh_info_outputdir + "/output.valid01.xmsh";
  fs::create_directories(xmsh_info_outputdir);
  xmsh::write(outfilename, xmsh_info, exact_xmsh);

  auto const xmsh = xmsh::read_xmsh<A>(outfilename);
  REQUIRE(xmsh == exact_xmsh);
}
