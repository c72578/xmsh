#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <experimental/filesystem>
#include <fstream>
#include <testxmsh/meta/testpath.hpp>
#include <testxmsh/unit/detail/streamio/xmsh_info.hpp>
#include <xmsh/detail/streamio/xmsh_info.hpp>
#include <xmsh/enumeration.hpp>
namespace fs = std::experimental::filesystem;
using A = std::allocator<void>;

constexpr auto const reldir = "/unit/detail/streamio/xmshinfo";

TEST_CASE("xmsh_info: Read Valid 01")
{
  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testdatadir = testpath.data;
  auto xmsh_info_datadir = std::string(testdatadir) + std::string(reldir);

  auto const validinput01 = xmsh_info_datadir + "/validinput01.xmsh";

  std::cout << validinput01 << std::endl;
  REQUIRE(fs::exists(validinput01));
  std::ifstream infile(validinput01);
  auto const xmsh_info = xmsh::detail::streamio::read_xmsh_info(infile, A());
  auto const exact_xmsh_info
    = testxmsh::unit::detail::streamio::generate_xmsh_info_valid01();
  REQUIRE(xmsh_info == exact_xmsh_info);
}

TEST_CASE("xmsh_info: Write Valid 01")
{
  auto const exact_xmsh_info
    = testxmsh::unit::detail::streamio::generate_xmsh_info_output_valid01();

  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testoutputdir = testpath.output;
  auto xmsh_info_outputdir = std::string(testoutputdir) + std::string(reldir);

  auto const outfilename = xmsh_info_outputdir + "/output.valid01.xmsh";
  fs::create_directories(xmsh_info_outputdir);
  std::ofstream outfile(outfilename);
  xmsh::detail::streamio::write(outfile, exact_xmsh_info);
  outfile.close();

  std::ifstream infile(outfilename);
  auto const xmsh_info = xmsh::detail::streamio::read_xmsh_info(infile, A());
  REQUIRE(xmsh_info == exact_xmsh_info);
}
