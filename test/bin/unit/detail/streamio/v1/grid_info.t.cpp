#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <experimental/filesystem>
#include <fstream>
#include <testxmsh/meta/testpath.hpp>
#include <testxmsh/unit/detail/streamio/v1/grid_info.hpp>
#include <xmsh/detail/streamio/v1/grid_info.hpp>
#include <xmsh/enumeration.hpp>
namespace fs = std::experimental::filesystem;
using A = std::allocator<void>;

constexpr auto const reldir = "/unit/detail/streamio/v1/gridinfo";

TEST_CASE("grid_info: Read Valid 01")
{
  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testdatadir = testpath.data;
  auto grid_info_datadir = std::string(testdatadir) + std::string(reldir);

  auto const validinput01 = grid_info_datadir + "/validinput01.xmsh";

  std::cout << validinput01 << std::endl;
  REQUIRE(fs::exists(validinput01));
  std::ifstream infile(validinput01);
  auto const grid_info
    = xmsh::detail::streamio::v1::read_grid_info(infile, A());
  auto const exact_grid_info
    = testxmsh::unit::detail::streamio::generate_grid_info_valid01();
  REQUIRE(grid_info == exact_grid_info);
}

TEST_CASE("grid_info: Write Valid 01")
{
  auto const exact_grid_info
    = testxmsh::unit::detail::streamio::generate_grid_info_output_valid01();

  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testoutputdir = testpath.output;
  auto grid_info_outputdir = std::string(testoutputdir) + std::string(reldir);

  auto const outfilename = grid_info_outputdir + "/output.valid01.xmsh";
  fs::create_directories(grid_info_outputdir);
  std::ofstream outfile(outfilename);
  xmsh::detail::streamio::v1::write(outfile, exact_grid_info);
  outfile.close();

  std::ifstream infile(outfilename);
  auto const grid_info
    = xmsh::detail::streamio::v1::read_grid_info(infile, A());
  REQUIRE(grid_info == exact_grid_info);
}
