#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <testxmsh/meta/testpath.hpp>
#include <testxmsh/unit/detail/streamio/v1/grid_data.hpp>
#include <xmsh/detail/streamio/v1/grid_data.hpp>
#include <xmsh/enumeration.hpp>

namespace fs = std::experimental::filesystem;
using A = std::allocator<void>;

constexpr auto const reldir = "/unit/detail/streamio/v1/griddata";

TEST_CASE("grid_data: Read Valid 01")
{
  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testdatadir = testpath.data;
  auto xmsh_info_datadir = std::string(testdatadir) + std::string(reldir);

  auto const validinput01 = xmsh_info_datadir + "/validinput01.xmsh";

  REQUIRE(fs::exists(validinput01));
  std::ifstream infile(validinput01);
  auto const grid_data
    = xmsh::detail::streamio::v1::read_grid_data<xmsh::data_type::fp, A>(infile,
                                                                         A());
  auto const exact_grid_data
    = testxmsh::unit::detail::streamio::v1::generate_grid_data_valid01();
  REQUIRE(grid_data == exact_grid_data);
}

TEST_CASE("grid_data: Write Valid 01")
{
  auto const exact_grid_data
    = testxmsh::unit::detail::streamio::v1::generate_grid_data_output_valid01();

  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testoutputdir = testpath.output;
  auto outputdir = std::string(testoutputdir) + std::string(reldir);

  auto const outfilename = outputdir + "/output.valid01.xmsh";
  fs::create_directories(outputdir);
  std::ofstream outfile(outfilename);
  outfile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
  outfile << std::scientific;

  xmsh::detail::streamio::v1::write(outfile, exact_grid_data);
  outfile.close();

  std::ifstream infile(outfilename);
  auto const grid_data
    = xmsh::detail::streamio::v1::read_grid_data<xmsh::data_type::fp, A>(infile,
                                                                         A());
  REQUIRE(grid_data == exact_grid_data);
}
