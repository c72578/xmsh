#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <experimental/filesystem>
#include <fstream>
#include <iomanip>
#include <testxmsh/meta/testpath.hpp>
#include <testxmsh/unit/detail/streamio/v1/vertex_array.hpp>
#include <xmsh/detail/streamio/v1/vertex_array.hpp>
#include <xmsh/enumeration.hpp>

namespace fs = std::experimental::filesystem;
using A = std::allocator<void>;

constexpr auto const reldir = "/unit/detail/streamio/v1/vertexarray";

TEST_CASE("vertex_array: Read Valid 01")
{
  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testdatadir = testpath.data;
  auto xmsh_info_datadir = std::string(testdatadir) + std::string(reldir);

  auto const validinput01 = xmsh_info_datadir + "/validinput01.xmsh";

  REQUIRE(fs::exists(validinput01));
  std::ifstream infile(validinput01);

  auto const vertex_array
    = xmsh::detail::streamio::v1::read_vertex_array(infile, A());
  auto const exact_vertex_array
    = testxmsh::unit::detail::streamio::v1::generate_vertex_array_valid01();
  REQUIRE(vertex_array == exact_vertex_array);
}

TEST_CASE("vertex_array: Write Valid 01")
{
  auto const exact_vertex_array = testxmsh::unit::detail::streamio::v1::
    generate_vertex_array_output_valid01();

  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testoutputdir = testpath.output;
  auto outputdir = std::string(testoutputdir) + std::string(reldir);

  auto const outfilename = outputdir + "/output.valid01.xmsh";
  fs::create_directories(outputdir);
  std::ofstream outfile(outfilename);
  outfile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
  outfile << std::scientific;
  xmsh::detail::streamio::v1::write(outfile, exact_vertex_array);
  outfile.close();

  std::ifstream infile(outfilename);
  auto const& vertex_array
    = xmsh::detail::streamio::v1::read_vertex_array(infile, A());
  REQUIRE(vertex_array == exact_vertex_array);
}
