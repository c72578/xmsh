#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <experimental/filesystem>
#include <fstream>
#include <testxmsh/meta/testpath.hpp>
#include <testxmsh/unit/detail/streamio/v1/file_array.hpp>
#include <xmsh/detail/streamio/v1/file_array.hpp>
#include <xmsh/enumeration.hpp>

namespace fs = std::experimental::filesystem;
using A = std::allocator<void>;

constexpr auto const reldir = "/unit/detail/streamio/v1/filearray";

TEST_CASE("file_array: Read Valid 01")
{
  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testdatadir = testpath.data;
  auto xmsh_info_datadir = std::string(testdatadir) + std::string(reldir);

  auto const validinput01 = xmsh_info_datadir + "/validinput01.xmsh";

  REQUIRE(fs::exists(validinput01));
  std::ifstream infile(validinput01);
  auto const filearray
    = xmsh::detail::streamio::v1::read_file_array(infile, A());
  auto const exact_filearray
    = testxmsh::unit::detail::streamio::v1::generate_filearray_valid01();
  REQUIRE(filearray == exact_filearray);
}

TEST_CASE("file_array: Write Valid 01")
{
  auto const exact_filearray
    = testxmsh::unit::detail::streamio::v1::generate_filearray_output_valid01();

  constexpr auto testpath = testxmsh::meta::TestPath();
  constexpr auto testoutputdir = testpath.output;
  auto outputdir = std::string(testoutputdir) + std::string(reldir);

  auto const outfilename = outputdir + "/output.valid01.xmsh";
  fs::create_directories(outputdir);
  std::ofstream outfile(outfilename);
  xmsh::detail::streamio::v1::write(outfile, exact_filearray);
  outfile.close();

  std::ifstream infile(outfilename);
  auto const filearray
    = xmsh::detail::streamio::v1::read_file_array(infile, A());
  REQUIRE(filearray == exact_filearray);
}
