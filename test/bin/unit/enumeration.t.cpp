#define CATCH_CONFIG_FAST_COMPILE
#include <catch2/catch.hpp>
#include <testxmsh/example.hpp>
#include <xmsh/enumeration.hpp>

TEST_CASE("Enumeration: Constructor from UT", "[constructor]")
{
  using Example = testxmsh::Example;
  auto const example = xmsh::to_enumeration<Example>(20);
  REQUIRE(example == Example::EO);
}

TEST_CASE("Enumeration: Constructor from string", "[constructor]")
{
  using Example = testxmsh::Example;
  auto const example = xmsh::to_enumeration<Example>("Godunov");
  REQUIRE(example == Example::Godunov);
}
