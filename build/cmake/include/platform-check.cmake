include(CheckTypeSize)
check_type_size("int64_t" SIZEOF_INT64_T LANGUAGE CXX)

if(NOT (SIZEOF_INT64_T EQUAL "8"))
  message(FATAL_ERROR "Can't find int64_t")
endif()
