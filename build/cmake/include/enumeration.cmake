function(target_sources_enumeration TARGETNAME INFILE)
  # Generate File
  get_filename_component(ABS_INFILE ${INFILE} ABSOLUTE)
  file(RELATIVE_PATH REL_INFILE ${PROJECT_SOURCE_DIR} ${ABS_INFILE})
  set(OUTFILERAW "${PROJECT_BINARY_DIR}/gen/${REL_INFILE}")
  string(REPLACE ".enum" ".hpp" OUTFILE ${OUTFILERAW})
  file(RELATIVE_PATH REL_OUTFILE ${PROJECT_BINARY_DIR} ${OUTFILE})
  add_custom_command(
    OUTPUT ${OUTFILE}
    COMMAND ${PYTHON3_EXECUTABLE} ${PROJECT_SOURCE_DIR}/bin/enumeration.py
      --infilename ${ABS_INFILE}
      --srcdir ${PROJECT_SOURCE_DIR}
      --outdir ${PROJECT_BINARY_DIR}/gen
    DEPENDS ${INFILE} ${PROJECT_SOURCE_DIR}/bin/enumeration.py
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    COMMENT "Generating enum header ${REL_OUTFILE}"
  )

  string(REPLACE "/" "_" OUTTARGET ${REL_OUTFILE})
  string(REPLACE "." "_" OUTTARGET ${OUTTARGET})
  add_custom_target(
    ${OUTTARGET}
    DEPENDS ${OUTFILE}
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    )

  add_dependencies(${TARGETNAME} ${OUTTARGET})
endfunction()
