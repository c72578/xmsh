include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion
  )

configure_package_config_file(
  ${PROJECT_SOURCE_DIR}/build/cmake/configure/VersionConfig.cmake.in
  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
  INSTALL_DESTINATION ${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}
  )

configure_file(
  ${PROJECT_SOURCE_DIR}/build/cmake/configure/project_info.hpp.in
  ${PROJECT_BINARY_DIR}/gen/src/include/xmsh/meta/project_info.hpp
  )

target_include_directories(
  ${LIBNAME}
  PUBLIC
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/gen/src/include>
  $<INSTALL_INTERFACE:include>
  )

