# Configure Different Level of Tests
# code -> the mandatory level (short tests)
# math -> ensure that the building blocks perform correctly (slightly longer)
# algorithms -> ensure that the algorithms are working as expected (long tests)
#  Documentation Building Option
option(BUILD_TESTING "Build Testing" OFF)

set(XMSH_TEST "None" CACHE STRING "Selection of Tests")
set_property(CACHE XMSH_TEST PROPERTY STRINGS None Code)


if (
    BUILD_TESTING
    AND (${PROJECT_SOURCE_DIR} STREQUAL ${CMAKE_SOURCE_DIR})
    AND (${XMSH_TEST} STREQUAL "None")
    )
  message("-- BUILD_TESTING is On but XMSH_TEST is set to None.")
  message("-- Setting XMSH_TEST to Code")
  set(XMSH_TEST "Code")
endif()

if (NOT (${XMSH_TEST} STREQUAL "None"))
  # Check that XMSH_TESTDATA_PATH is not empty
  if (NOT DEFINED XMSH_TESTDATA_PATH)
    message(FATAL_ERROR "XMSH_TESTDATA_PATH is required to be specified")
  endif()

  # Check that XMSH_TESTOUTPUT_PATH is not empty
  if (NOT DEFINED XMSH_TESTOUTPUT_PATH)
    message(FATAL_ERROR "XMSH_TESTDATA_PATH is required to be specified")
  endif()

  get_filename_component(
    XMSH_TESTDATA_ABSPATH
    ${XMSH_TESTDATA_PATH}
    ABSOLUTE
    )
  get_filename_component(
    XMSH_TESTOUTPUT_ABSPATH
    ${XMSH_TESTOUTPUT_PATH}
    ABSOLUTE
    )
endif()

#  Documentation Building Option
option(BUILD_DOC "Build Documentation" ON)
