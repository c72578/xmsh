# 

2019-05-09 16:36:50 UTC

- [\[1c835c8\]](http://github.com/nagzira/xmsh/commit/1c835c828a1dfb95e79317b28f3d10685681921e)  + meta cmake : correct license files
- [\[525e0d5\]](http://github.com/nagzira/xmsh/commit/525e0d5aa104e2693e951cc52e0907fbcaf66f48)  + meta ci : added support for CircleCI
- [\[c1b2ecb\]](http://github.com/nagzira/xmsh/commit/c1b2ecbf1016f63572afb15e808994f04fdee17b)  - devtools jayeshbadwaik : updated ycm_extra_conf.py
- [\[01d0909\]](http://github.com/nagzira/xmsh/commit/01d0909cfcf909edb4b82e9190c26f67e2cbf74c)  + dependencies : added dependency on nlohmann_json
- [\[a9f80db\]](http://github.com/nagzira/xmsh/commit/a9f80db4f58e59a6111629b9c69c635f597ef2af)  - cmake : removed old python detection code
- [\[34b0311\]](http://github.com/nagzira/xmsh/commit/34b0311d276e84522c2743aa955bb629caf1c739)  - build : moved dependency on catch2 to external package
- [\[99e2379\]](http://github.com/nagzira/xmsh/commit/99e2379bd722ca2901ec3f0d014f2cc090e172cf)  - readme : corrected CI status and links

# v0.5.1

2019-01-19 12:54:31 UTC

- [\[72ee4f2\]](http://github.com/nagzira/xmsh/commit/72ee4f2376a13e155130d759b5cb7b092624610a)  - devtools jayeshbadwaik : ycm file
- [\[bb26788\]](http://github.com/nagzira/xmsh/commit/bb267886e1333c2cc165f90c0dd84faf9d840ca5)  + meta cmake : use HOMEPAGE_URL provided since cmake 3.12

# v0.5

2019-01-11 06:03:58 UTC

- [\[dfa3226\]](http://github.com/nagzira/xmsh/commit/dfa3226f8c7a5c1d2b1cbabba9f297e750381340)  + use uses_allocator uniformly
- [\[0ceb446\]](http://github.com/nagzira/xmsh/commit/0ceb4469ec2915be45f5dd24c99052cd51faede5)  + devtools jayeshbadwaik : fix clang-tidy and include paths
- [\[fa70bd7\]](http://github.com/nagzira/xmsh/commit/fa70bd727f04f4e0d3d23b1bc78b7ea7d35c85f7)  - detail streamio v1 : fix clang-tidy bug
- [\[4ccd19c\]](http://github.com/nagzira/xmsh/commit/4ccd19cea47c7b57202df2720108177bccfff363)  + legal : update license
- [\[f19c8fd\]](http://github.com/nagzira/xmsh/commit/f19c8fdbb0dbc05218dff3f1db31e1cfd3041845)  - rename XMSH to XMSHData
- [\[21806b0\]](http://github.com/nagzira/xmsh/commit/21806b0b14b392113e3d89c1a252cbaeec41f8f9)  - legal : fix license install
- [\[cdc91ad\]](http://github.com/nagzira/xmsh/commit/cdc91adbfdb03adb2ae31bc8d57cd33179dadc9d)  - meta cmake : fix version number in CMakeLists file
- [\[e6e740c\]](http://github.com/nagzira/xmsh/commit/e6e740cce26312d11a10062eac80613ecef96467)  + third_party/catch2 : update to v2.4.2
- [\[17d038d\]](http://github.com/nagzira/xmsh/commit/17d038d840270e39cb6e08d04135c459c994c7cb)  - lowercase names : XMSHData to xmsh_data
- [\[e8eca1d\]](http://github.com/nagzira/xmsh/commit/e8eca1dccda2656e53b4edc3cb0b0c8b854ea294)  - enumeration : export lowercase types
- [\[7a99698\]](http://github.com/nagzira/xmsh/commit/7a996989bf906426247a7c79fa838a7d541254ef)  - cleanup :  lowercase DataType to data_type
- [\[206161e\]](http://github.com/nagzira/xmsh/commit/206161e2937c070ddbf73e286cc2e0b18ac7e7bc)  - cleanup : lowercase file_type
- [\[3d2ae25\]](http://github.com/nagzira/xmsh/commit/3d2ae250e0d1a5bf93ceb5b10584d8b48d6ac282)  - cleanup : ElementType to element_type
- [\[349fb91\]](http://github.com/nagzira/xmsh/commit/349fb915dcd09a6467f07412c92b317f3648f503)  - cleanup : GridDim to grid_dimension
- [\[3556d36\]](http://github.com/nagzira/xmsh/commit/3556d36fbed010d519aaacdc478cdaa31ca63cbf)  - cleanup : MultiFile to multifile
- [\[31d5973\]](http://github.com/nagzira/xmsh/commit/31d597314c0d900980923cc8f5be180fd0bb38e8)  - cleanup : IOType to io_type
- [\[e0c8f6a\]](http://github.com/nagzira/xmsh/commit/e0c8f6a53d1db637d38b14f86a8d327cf589261b)  - cleanup : SectionType to section_type
- [\[805285b\]](http://github.com/nagzira/xmsh/commit/805285b92eafbc0915a9e19838c2cc75e0e1dcc1)  - meta cmake : fix filenames
- [\[f30f3a4\]](http://github.com/nagzira/xmsh/commit/f30f3a41f0b74c1174756656e37238e9ae182472)  - bug triaging
- [\[c58a09a\]](http://github.com/nagzira/xmsh/commit/c58a09aee3d78cfd97874c8790d89cbe2745f347)  - ProjectInfo to project_info
- [\[6f1d499\]](http://github.com/nagzira/xmsh/commit/6f1d4996d5a01ecbc86eecabca880cd063520658)  - WorldDim to world_dimension
- [\[20d44bb\]](http://github.com/nagzira/xmsh/commit/20d44bbc8940c91354b26172358851b33f065666)  - meta buildinfo : disambiguate typename from member name
- [\[7675b66\]](http://github.com/nagzira/xmsh/commit/7675b666d544fa0993a5ed19484274fb91f82db6)  - Preamble to preamble
- [\[e163749\]](http://github.com/nagzira/xmsh/commit/e163749d586afd4cdb8d946a65ff2152cf38379d)  - ElementArray to element_array
- [\[e7b9779\]](http://github.com/nagzira/xmsh/commit/e7b9779b581a5cca6cdaa998c41d7cc740aa881b)  - prevent preamble shadowing
- [\[e61a551\]](http://github.com/nagzira/xmsh/commit/e61a551f08ea9ea0545656ff3d2538f56e5b0f3b)  - clang-tidy corrections
- [\[6653f9e\]](http://github.com/nagzira/xmsh/commit/6653f9eeea4bcbf1dc7a8f329fac0bdb51839c74)  - FileArray to file_array
- [\[0c3bef5\]](http://github.com/nagzira/xmsh/commit/0c3bef50d96e8e09f0b5ec1f71493324a9325ebd)  - FileInfo to file_info
- [\[d6de544\]](http://github.com/nagzira/xmsh/commit/d6de544d4d2f9c76e168fc59f8da34ea2489289c)  - ElementData to element_data
- [\[7bdbcfe\]](http://github.com/nagzira/xmsh/commit/7bdbcfe901079fb14ef65427141581b3c106d98e)  - VertexArray to vertex_array
- [\[a256974\]](http://github.com/nagzira/xmsh/commit/a256974a12001ad9a93872e70dedca14cfa3392c)  + GridData to grid_data
- [\[db67893\]](http://github.com/nagzira/xmsh/commit/db67893891edbc23fdf37c873c6628fd8dc5f55e)  - header guard fix
- [\[98b4b89\]](http://github.com/nagzira/xmsh/commit/98b4b899da6ae96bda29ca08de431fe6ffd38410)  - correct some filenames
- [\[32917d0\]](http://github.com/nagzira/xmsh/commit/32917d0e9819303972293d49186d923681ffb73c)  - GridInfo to grid_info
- [\[f7dcc6d\]](http://github.com/nagzira/xmsh/commit/f7dcc6d40e465f819a36698f23ef5e2914a74fe4)  - fix names to disambiguate typename from member variable names
- [\[87f097b\]](http://github.com/nagzira/xmsh/commit/87f097b85db41e12eff0a135fcd39af6ca40ccf9)  - XMSHInfo to xmsh_info
- [\[8e23ae1\]](http://github.com/nagzira/xmsh/commit/8e23ae11c6419acdc20f712d0362b5701e384515)  - enumeration has its own error code enum
- [\[309e2a7\]](http://github.com/nagzira/xmsh/commit/309e2a722e1d369a861d0bfc50126aa174ef9fb6)  - convert from std::error_code to custom reflection based error_code
- [\[b1da211\]](http://github.com/nagzira/xmsh/commit/b1da211ab5872838e29eac09887aa640e8ff00cb)  - remove error_code constructs
- [\[31987f5\]](http://github.com/nagzira/xmsh/commit/31987f5504eaefb84a3f8dbeb39296e2e8ada9ed)  + enumeration : to_qualified_string_view
- [\[e4898b1\]](http://github.com/nagzira/xmsh/commit/e4898b1e14ec713701a2f3e4192a62fe5a75d678)  + better typename for buildinfo
- [\[4bccd8c\]](http://github.com/nagzira/xmsh/commit/4bccd8ceb1f317c9f637983973732a317544b91b)  + meta : custom behavior on encountering library bug
- [\[356dd0e\]](http://github.com/nagzira/xmsh/commit/356dd0ed64fd215a20075f79fcbc5b0ae87f9d81)  - meta : use qualified function calls to disable ADL
- [\[7fdc3ef\]](http://github.com/nagzira/xmsh/commit/7fdc3ef63381bf3ad21563ba0edbb1ae209cef86)  - grid_data : fixed support for codim=dim data
- [\[0b5e5f0\]](http://github.com/nagzira/xmsh/commit/0b5e5f038784831eea2d83d92f41e334891965a3)  - meta ci : fix gitlab ci
- [\[eb5279d\]](http://github.com/nagzira/xmsh/commit/eb5279d3d6b4be1a5b16df13e2a20a4554ea5bcf)  - enumeration : use exception for error handling instead of expected
- [\[8f537de\]](http://github.com/nagzira/xmsh/commit/8f537dec177b62a092ca3294bb558f390d8aa3c1)  - detail streamio v1 section_type : use expections instead of expected
- [\[ee663ae\]](http://github.com/nagzira/xmsh/commit/ee663aeb8ce6787ed462f0d8f2ffadd5de96b0c1)  - detail streamio v1 vertex_array : expected to exceptions
- [\[cdd5397\]](http://github.com/nagzira/xmsh/commit/cdd5397ec008c9b068cbaa9d3f25981151a2cc2c)  - detail streamio v1 element_array : expected to exceptions
- [\[fb348fe\]](http://github.com/nagzira/xmsh/commit/fb348fe68f97c9bb485f35084bbf1d741f04d393)  - detail streamio v1 grid_data : expected to exceptions
- [\[0b00c3c\]](http://github.com/nagzira/xmsh/commit/0b00c3c2e82178a7834bc8efaf1bbccc9083a371)  - detail streamio v1 : remove pessimizing std::move
- [\[72ca0a4\]](http://github.com/nagzira/xmsh/commit/72ca0a474dc0513f0ac87f23bc1f4cd472d17ccd)  - detail streamio v1 file_array : expected to exceptions
- [\[922c2e1\]](http://github.com/nagzira/xmsh/commit/922c2e131aa62b05f93538971bc86c0015dbfda4)  - detail streamio v1 preamble : expected to exceptions
- [\[493d87e\]](http://github.com/nagzira/xmsh/commit/493d87e034a5d2a03ddf91aed0e46d8f9a1ba7c5)  - enumeration : remove spurious comments
- [\[b4c60b7\]](http://github.com/nagzira/xmsh/commit/b4c60b7b938d8b3871a4c1b7fd1d007d858afb59)  - meta : remove extraneous comments
- [\[fc4db82\]](http://github.com/nagzira/xmsh/commit/fc4db822868c96e7dfb0e4ba51513234f53dd669)  - detail streamio v1 grid_info : expected to exceptions
- [\[8c6dd4a\]](http://github.com/nagzira/xmsh/commit/8c6dd4a27593ea70076be733fb3f7e9d44004991)  - detail streamio v1 xmsh : expected to exceptions
- [\[6c77ad6\]](http://github.com/nagzira/xmsh/commit/6c77ad692910341b65d9d017ed80258b9dbe3bf2)  - detail streamio xmsh : expected to exceptions
- [\[d280811\]](http://github.com/nagzira/xmsh/commit/d280811c1ce22296ce91323158ff019586fdbca4)  - xmsh : expected to exceptions
- [\[8518b90\]](http://github.com/nagzira/xmsh/commit/8518b904c2a47d921b029ef76432dcc601cf778c)  - change return type of write methods to void
- [\[d7342cb\]](http://github.com/nagzira/xmsh/commit/d7342cb8e2312b6550520b82402fa5ea68311b1b)  - detail streamio xmsh_info : expected to exceptions
- [\[25103eb\]](http://github.com/nagzira/xmsh/commit/25103ebc1838fedee30d0af7935dc77b896c5172)  - tl/expected : remove expected from code usage
- [\[8577160\]](http://github.com/nagzira/xmsh/commit/8577160698d3de479c7c3e0cf15b144796e22f67)  - enumeration : removed type no longer required
- [\[c963958\]](http://github.com/nagzira/xmsh/commit/c963958de67012b4a2120fb82d7106b4c8aab190)  - 64 bit integer data
- [\[7aa2e49\]](http://github.com/nagzira/xmsh/commit/7aa2e493711bba0db486beee644601baf7caf398)  - element_data : fix comparison
- [\[ce68377\]](http://github.com/nagzira/xmsh/commit/ce6837744720a4f1eaa8b40c8e1517a98773177e)  + meta cmake : add check for int64_t support
- [\[082df15\]](http://github.com/nagzira/xmsh/commit/082df156abedaa48aefe2e7c46c3456243aaee39)  - use std namespaced int64_t
- [\[5b455cd\]](http://github.com/nagzira/xmsh/commit/5b455cdeb6ddb3d86606de370de47b526e639b66)  - change vertex_list to vertex_array
- [\[14f7f6b\]](http://github.com/nagzira/xmsh/commit/14f7f6b4bc408a78e53fc5ef4c585220856ba67d)  - remove default constructible condition on allocator
- [\[ea52970\]](http://github.com/nagzira/xmsh/commit/ea52970998a9040a9c11d70ebe5d67d67f0873ee)  - throw on file not found
- [\[fd13ae5\]](http://github.com/nagzira/xmsh/commit/fd13ae5823aefd3f35499d80e83224feafc751af)  - test unit detail streamio v1 : uninclude tl/expected headers
- [\[da44fec\]](http://github.com/nagzira/xmsh/commit/da44fecdaa104cf0f368a7445e96464249974954)  - meta ci : fix ci to use arch-hpcstack
- [\[7f97db8\]](http://github.com/nagzira/xmsh/commit/7f97db8c29cc67697546a1145e1c2f7d1e724b9e)  - doc web : fix installation instructions
- [\[0609b17\]](http://github.com/nagzira/xmsh/commit/0609b17a97a35ec5b149805b8675ea6ffb8ebeec)  - doc web install : remove misleading information
- [\[58de7fe\]](http://github.com/nagzira/xmsh/commit/58de7fe39b8da4d36f67ab8d74786cb87f65be8d)  - meta ci : remove XMSH_TERMINATE_ON_BUG feature
- [\[5fa73b5\]](http://github.com/nagzira/xmsh/commit/5fa73b526ef5ac2b40c906f35ad1f4f7c070913f)  - meta legal : corrected a typo and added a COPYRIGHT FILE
- [\[663113b\]](http://github.com/nagzira/xmsh/commit/663113b61a8853a915e187ae599389549d118f45)  - meta : lower case filenames

# v0.4.1

2018-11-08 18:28:36 UTC



# v0.4

2018-11-08 16:07:45 UTC

- [\[0ab38df\]](http://github.com/nagzira/xmsh/commit/0ab38df3b526c347d382342beb9c0f1c181dbd55)  + devtools jayeshbadwaik : better include path
- [\[809a9af\]](http://github.com/nagzira/xmsh/commit/809a9af9b9301d35aa9c304829c8d8be459decac)  - return using std::move to workaround gcc bug
- [\[95e293e\]](http://github.com/nagzira/xmsh/commit/95e293e072899e5642285eb722ff52eb5ef553e7)  + meta tests : do catch testing correctly
- [\[0817d84\]](http://github.com/nagzira/xmsh/commit/0817d84174aa1d87c383444333c9e3dddaf94402)  - catch2 : define CATCH_CONFIG_FAST_COMPILE to reduce compile times

# v0.3.1

2018-11-08 04:40:47 UTC

- [\[d3978af\]](http://github.com/nagzira/xmsh/commit/d3978af37a1f3bafe86d03b07c5995f5ad12bf59)  + explicitly depend on tl/expected

# v0.3

2018-11-08 03:09:27 UTC

- [\[5ef230b\]](http://github.com/nagzira/xmsh/commit/5ef230b4afb192fa27f17b062c672b6aa3f1f47f)  - remove tex documentation
- [\[953fbe8\]](http://github.com/nagzira/xmsh/commit/953fbe84fb4c7845f3412d60d654ad551dd9b2b0)  + documentation : added a sample parallel file
- [\[95a93d4\]](http://github.com/nagzira/xmsh/commit/95a93d4d5be11d124366339a5c5200654bfb0215)  - make monadic use of `tl::expected`
- [\[980122d\]](http://github.com/nagzira/xmsh/commit/980122dd27320cce5868fe4d07573e8084c98486)  - make private headers public
- [\[bf60c17\]](http://github.com/nagzira/xmsh/commit/bf60c1792aa7de7659a495f88984bf7db0d9d609)  - put sources in `lib` folder instead of `lib/src`
- [\[0839cba\]](http://github.com/nagzira/xmsh/commit/0839cbaa8f2c14212c8d4696686f6ef8422413d5)  * remove expected as third party
- [\[0318607\]](http://github.com/nagzira/xmsh/commit/0318607ec05050420fbfd9fff9599a6062e03782)  * remove toolchain files
- [\[f211483\]](http://github.com/nagzira/xmsh/commit/f211483ad17b8660a912940aaca57343146133c0)  - meta : fix filesystem linking

# v0.2.3

2018-11-06 12:49:27 UTC

- [\[269a175\]](http://github.com/nagzira/xmsh/commit/269a175092f976c380f6d9e136e37cbd56329394)  - meta cmake : install correct xmsh cmake config file

# v0.2.2

2018-11-05 19:40:41 UTC

- [\[b1f521e\]](http://github.com/nagzira/xmsh/commit/b1f521eacfe7da1060d3e69252ee134cc9087057)  + meta : add library support for vcpkg

# v0.2.1

2018-11-05 18:57:14 UTC

- [\[00b4a11\]](http://github.com/nagzira/xmsh/commit/00b4a11db3b97e7f90c0beb86421ecbed9e2e800)  - meta cmake : support cmake 3.11

# v0.2

2018-11-05 14:47:12 UTC

- [\[2993e33\]](http://github.com/nagzira/xmsh/commit/2993e33fab08d945cf32aa63bef014a82e7e5cdf)  * changelog : modify date style in change log
- [\[389106e\]](http://github.com/nagzira/xmsh/commit/389106e4bd19dd48de9bd093885ef3044a0fc7dd)  + elementarray : template instantiations for std::allocator
- [\[6366e8e\]](http://github.com/nagzira/xmsh/commit/6366e8eccaac2d37fd1141b04e1879aca5d791ca)  + griddata and elementdata : instantiation for std::allocator
- [\[c7b1fde\]](http://github.com/nagzira/xmsh/commit/c7b1fde2e0588195fb22d83c637e9be6547c9d83)  + filearray : instantiation for std::allocator
- [\[0bb480e\]](http://github.com/nagzira/xmsh/commit/0bb480e3ff4d09e9a48fc0ba16e72ff3d01a3048)  + vertexarray : instantiation for std::allocator
- [\[120f442\]](http://github.com/nagzira/xmsh/commit/120f442f99a12375abd6edb93475620f11c116d6)  = elementarray : source reformat
- [\[d645da4\]](http://github.com/nagzira/xmsh/commit/d645da4220b4b48176b16118e545a5da505a5913)  + gridinfo : instantiation for std::allocator
- [\[f76e21f\]](http://github.com/nagzira/xmsh/commit/f76e21f5985f31bf627856a14905a60b6be5b037)  - xmshdata : fix constructor bug
- [\[0f70434\]](http://github.com/nagzira/xmsh/commit/0f70434fdce636fd012909960fa9351779d26177)  + xmsh : instantiation for std::allocator
- [\[401507d\]](http://github.com/nagzira/xmsh/commit/401507d3739ea5770969d22f30c75471639b41cd)  + testxmsh : compile test01 and test02
- [\[dda61ed\]](http://github.com/nagzira/xmsh/commit/dda61edb73759f10e875fdfbd55d1bf9284f65b3)  - bugfix : fix header includes
- [\[b054c43\]](http://github.com/nagzira/xmsh/commit/b054c43175931d16279618ea34078ce0828f6542)  - meta : fix changelog generator
- [\[575e7ea\]](http://github.com/nagzira/xmsh/commit/575e7eafe2c6f2703646288d7944711ce937f7d2)  + v0.2

# v0.1.2

Sun 4 Nov 20:12:36 UTC 2018

- [\[c7302db\]](http://github.com/nagzira/xmsh/commit/c7302dbf0e61a47cc6e64d528b5af33edb339ca6)  - meta ci formatting : make formatting more robust/safe
- [\[b8d6a3c\]](http://github.com/nagzira/xmsh/commit/b8d6a3c23bbdb0eb9aed74bbc396a66ca70b51a0)  - meta cmake : change library name to xmsh
- [\[82695cc\]](http://github.com/nagzira/xmsh/commit/82695ccdeb6db189146eb7b81fd3c16e1c663050)  - meta cmake : cleanup cmake config
