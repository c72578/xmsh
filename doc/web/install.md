# Installation Instructions

These instructions are only for installation. In case, you want to build and
test the software, you should go to [Instructions for Developers](developer.md).

## Requirements

Installation of xmsh requires:
1. C++17 compiler. Supported compilers are:
    1.  llvm-6.0 and later
    2.  gcc 7.0 and later
2. Python 3 Interpreter with Standard Library

Optional Dependencies:
1. Doxygen (to build part of documentation)

# Instructions

We recommend out of source/out of directory builds. An example to build the library is as follows:

```
$ pushd <to-an-empty-directory>
$ git clone https://github.com/nagzira/xmsh.git
$ mkdir build
$ cmake ../xmsh
$ make
$ popd
```
