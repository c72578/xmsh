# Instructions for Developers

If you wish to do more than just install the library, and instead develop or
test it yourself, then there are a few more steps to follow:

The test data for the repository is stored outside in a separate repository.
The tests also produce file output which might be substantial in size, hence,
we specify a separate test output directory as well.

The test data repository is found at

```
https://github.com/nagzira/xmsh-testdata.git
```

The following instructions will download and build the xmsh library and the
corresponding tests. The following variables need to be specified manually
in this case:

0. `BUILD_TESTING` needs to be `On`
1. `XMSH_TESTDATA_PATH` -> Path to Test Data (should point to a copy of
   repository mentioned above).
2. `XMSH_TESTOUTPUT_PATH` -> Path to Store Test Output


```
$ pushd <some-empty-directory>
$ git clone https://github.com/nagzira/xmsh.git
$ mkdir build
$ cmake \
  -DBUILD_TESTING=On \
  -DXMSH_TESTDATA_PATH=<path-to-xmsh-testdata-repository> \
  -DXMSH_TESTOUTPUT_PATH=../testoutput \
  ../xmsh
$ make
$ popd
```


