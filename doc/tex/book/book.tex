\documentclass[
 titlepage=false,
%  oneside,
 fontsize=10pt,
%  headings=optiontoheadandtoc,
  a4paper]
  {scrreprt}

% For final printing
% \usepackage[a4,frame,center]{crop}

%% Config TeX
\input{config}

% BibTeX
\usepackage[sorting=ydnt,bibstyle=alphabetic,
            citestyle=alphabetic,backend=biber]{biblatex}
\addbibresource{library.bib}

\setuptoc{toc}{leveldown}
\title{XMSH : A Lossless Mesh Document Format}
\subtitle{Document Format Description and Reference Implementation}
\author{Jayesh Badwaik}
\usepackage{pkg/jaytodo}
%\makeindex

\lstset {
basicstyle=\ttfamily,
breaklines=true,
frame=l,
framesep=4.5mm,
framexleftmargin=10pt,
fillcolor=\color{lightgray},
rulecolor=\color{cyan},
numbers=left,
xleftmargin=20pt,
framextopmargin=0pt
}

\newcommand{\xmsh}{\inlinecode{xmsh}}
\newcommand{\xmsp}{\inlinecode{xmsp}}

\newcommand{\fileinfo}{\texttt{fileinfo}}
\newcommand{\grid_info}{\texttt{grid_info}}
\newcommand{\vertexlist}{\texttt{vertexlist}}
\newcommand{\elementlist}{\texttt{elementlist}}
\newcommand{\datalist}{\texttt{datalist}}
\newcommand{\patchlist}{\texttt{patchlist}}

\newcommand{\tvs}{\textvisiblespace}

\begin{document}
\maketitle

\begin{abstract}

\end{abstract}
\tableofcontents
%\mainmatter
%################################################################################
\chapter{Specification of Document Format}
%################################################################################
The \xmsh{} document format specifies two different types of files
\begin{enumerate}
  \item Main File   (Extension \xmsh{})
  \item Patch File  (Extension \xmsp{})
\end{enumerate}
Each \xmsh{} document is described by a mandatory \xmsh{} file and
zero or more \xmsp{} files. Each file, whether it is an \xmsh{} or \xmsp{} file,
contains multiple independently parseable sections. There are six different
types of sections
\begin{enumerate}[itemsep=0pt]
  \item \fileinfo
  \item \grid_info
  \item \vertexlist
  \item \elementlist
  \item \datalist
  \item \patchlist
\end{enumerate}
We also define some quantities we will need for later:
\begin{enumerate}
  \item $\bbD$
    Set of Allowed DataType,
    $\bbD = \{\text{"float"} , \text{"int"} \}$

  \item $\bbF$
    Set of Allowed FileType,
    $\bbF = \{\text{"serial"} , \text{"parallel"} \}$

  \item $\bbN$
    Set of Allowed Indices,
    $\bbN = \{\ n : n \text{ is uint64\_t}\}$

  \item $\bbR$
    Set of Allowed Reals,
    $\bbR = \{ f : f \text{ is IEEE754::binary64\}}$

  \item $\bbP$
    Set of Allowed Patch Type,
    $\bbD = \{\text{"ASCII"} , \text{"BINARY"} \}$

  \item $\bfS(d)$
    Scalar Type,
    \begin{align}
    \bfS(d) =
      \begin{cases}
        \bbR \text{ for } d = \text{"float"} \\
        \bbZ \text{ for } d = \text{"int"}
      \end{cases}
    \end{align}


  \item $\bbT$
    Set of Allowed Types of Elements,
    \begin{align}
      T \in
      \begin{Bmatrix}
        10  & -- &\text{ "Vertex"} \\
        110 & -- &\text{ "Edge"} \\
        120 & -- &\text{ "Polygon"} \\
        121 & -- &\text{ "Triangle"} \\
        122 & -- &\text{ "Square"}
      \end{Bmatrix}
    \end{align}


 \item $\bbV$
    Set of Available Versions,
    $\bbV = \{ 1\}$

  \item $\bbW$
    Set of Allowed World Dimensions,
    $\bbW = \{ 1, 2,3\}$

  \item $\bbZ$
    Set of Allowed Integer,
    $\bbZ = { n : -2^{63}\leq n \leq 2^{63}-1}$
\end{enumerate}
At this point, we would like to point out that the
rules for validity of a file or a section are local in nature and do not depend
on the global file. As such, it is completely possible for each file and section
to be valid, even if, the resultant combined file is invalid.  We now describe
the rules for sections and then, we will return to describing the rules for
\xmsh{} and \xmsp{} file format.
%--------------------------------------------------------------------------------
\section{\fileinfo{}}
%%-------------------------------------------------------------------------------

A \fileinfo{} section is considered valid if it has the following form
\begin{lstlisting}[escapeinside={(*}{*)}]
$fileinfo
version     v
filetype    f
label       l
$endfileinfo
\end{lstlisting}
and the quantities therein satisfy the following additional properties
\begin{enumerate}
\item $\rmv \in \{1\}$
\item $\rmf \in \{\text{"serial"}, \text{"parallel"}\}$
\item \texttt{l} is a string without any whitespace in it
\end{enumerate}
%%-------------------------------------------------------------------------------

%--------------------------------------------------------------------------------
\section{\grid_info}
%%-------------------------------------------------------------------------------
A \grid_info{} section is considered valid if it has the following form
\begin{lstlisting}[escapeinside={(*}{*)}]
$grid_info
wdim    w
dim     d
$endgrid_info
\end{lstlisting}
and the quantities therein satisfy the following additional properties
\begin{enumerate}
\item  $\rmw \in \bbW$
\item  $\rmd \in \bbW \text{ and } \rmd < \rmw$
\end{enumerate}
%%-------------------------------------------------------------------------------

%--------------------------------------------------------------------------------
\section{\vertexlist}
%%-------------------------------------------------------------------------------
A \vertexlist{} section is considered valid if it has the following form
\begin{lstlisting}[escapeinside={(*}{*)}]
$vertexlist
wdim    (*$w\  (w \in \bbW)$*)
nvertex (*$n\  (n \in \bbN)$*)
(*$v_0 \quad \quad x_{0,0}\quad\quad x_{0,1} \quad\quad\ldots\quad x_{0,w-1}$*)
(*$v_1 \quad \quad x_{1,0}\quad\quad x_{1,1} \quad\quad\ldots\quad x_{1,w-1}$*)
...
(*$v_{n-1} \quad x_{n-1,0}\quad x_{n-1,1} \quad \ldots \quad  x_{n-1,w-1}$*)
$endvertexlist
\end{lstlisting}
and the quantities therein satisfy the following additional properties
\begin{enumerate}
\item  $w \in \bbW$
\item  $n \in \bbN$
\item  $v_i \in \bbN$
\item  $x_{i,j} \in \bbR$
\item
 $i \neq j  \implies v_i \neq v_j
\text{ and }
(x_{i,0}, x_{i,1}, \ldots, x_{i,w-1}) \neq (x_{j,0}, x_{j,1}, \ldots, x_{j,w-1})$
\end{enumerate}
%%-------------------------------------------------------------------------------

%--------------------------------------------------------------------------------
\section{\elementlist}
%%-------------------------------------------------------------------------------
An \elementlist{} section is considered valid if it has the following form
\begin{lstlisting}[escapeinside={(*}{*)}]
$elementlist
codim  (*$c$*)
nelem  (*$n$*)
(*$e_0 \quad \quad t_0 \quad \quad
m_0 \quad\quad v_{0,0}\quad\quad v_{0,1}\quad\quad\ldots\quad
v_{0,\mathrm{m_0-1}}$*)
(*$e_1 \quad \quad t_1 \quad \quad
m_1 \quad\quad v_{1,0}\quad\quad v_{1,1}\quad\quad\ldots\quad
v_{1,\mathrm{m_1-1}}$*)
...
(*$e_{n-1}
\quad t_{n-1}
\quad m_{n-1}
\quad v_{n-1,0}
\quad v_{n-1,1}
\quad \ldots
\quad  v_{n-1,m_{n-1}}$*)
$endelementlist
\end{lstlisting}
and the quantities therein satisfy the following additional properties
\begin{enumerate}
  \item $c \in \bbW$
  \item $n \in \bbN$
  \item $e_i \in \bbN$
  \item $t_i \in \bbT$
  \item $m_i \in \bbN$
  \item $v_i \in \bbN$
  \item
 $i \neq j  \implies e_i \neq e_j
\text{ and }
(v_{i,0}, v_{i,1}, \ldots, v_{i,w-1}) \neq (v_{j,0}, v_{j,1}, \ldots, v_{j,w-1})$
\end{enumerate}


%%-------------------------------------------------------------------------------

%--------------------------------------------------------------------------------
\section{\datalist}
%%-------------------------------------------------------------------------------
An \datalist{} section is considered valid if it has the following form
\begin{lstlisting}[escapeinside={(*}{*)}]
$datalist
datatype (*$d$*)
codim (*$c$*)
label (*$l$*)
nelem (*$n$*)
(*$e_0 \quad \quad m_0 \quad\quad x_{0,0}\quad\quad x_{0,1}\quad\quad\ldots\quad
x_{0,\mathrm{m_0-1}}$*)
(*$e_1 \quad \quad m_1 \quad\quad x_{1,0}\quad\quad x_{1,1}\quad\quad\ldots\quad
x_{1,\mathrm{m_1-1}}$*)
...
(*$e_{n-1} \quad m_{n-1}
\quad x_{n-1,0}
\quad x_{n-1,1}
\quad \ldots
\quad  x_{n-1,m_{n-1}}$*)
$enddatalist
\end{lstlisting}
and the quantities therein satisfy the following additional properties
\begin{enumerate}
  \item $d \in \bbD$
  \item $c \in \bbW$
  \item $l$ is a string with no whitespace characeters
  \item $n \in \bbN$
  \item $e_i \in \bbN$
  \item $m_i \in \bbN$
  \item $x_{i,j} \in \bfS(d)$
  \item $i \neq j  \implies e_i \neq e_j$
\end{enumerate}
%%-------------------------------------------------------------------------------

%--------------------------------------------------------------------------------
\section{\patchlist}
%%-------------------------------------------------------------------------------
An \patchlist{} section is considered valid if it has the following form
\begin{lstlisting}[escapeinside={(*}{*)}]
$datalist
npatch (*$n$*)
(*$p_0 \quad \quad t_{0} \quad \quad \text{rfp}_0$*)
(*$p_1 \quad \quad t_{1} \quad \quad \text{rfp}_1$*)
...
(*$p_{n-1} \quad t_{n-1} \quad \quad \text{rfp}_{n-1}$*)
$endpatchlist
\end{lstlisting}
and the quantities therein satisfy the following additional properties
\begin{enumerate}
  \item $p_i \in \bbN$
  \item $t_i \in \bbP$
  \item $\rmr\rmf\rmp_i$ is "/"-separated path
\end{enumerate}
%%-------------------------------------------------------------------------------


%--------------------------------------------------------------------------------
\section{\xmsp}
%%-------------------------------------------------------------------------------

A patch file (\xmsp{}) can be of two types.
\begin{enumerate}
  \item ASCII
  \item BINARY
\end{enumerate}

\subsection{ASCII}

An ASCII \xmsp{} file is considered valid if and only if it satisfies the following
conditions:
\begin{enumerate}
  \item It contains at most one \vertexlist{} section (and it should be valid if
    it exists).
  \item Characterize an \elementlist{} section by `\inlinecode{codim}`. Then,
    the patch file contains at most one section of \elementlist for each
    \inlinecode{codim}.
  \item Characterize a \datalist{} section by \inlinecode{codim,label} pair.
    Then the patch file contains at most one \datalist{} for any given
    \inlinecode{codim,label} pair.
  \item It contains no other information.
\end{enumerate}


\subsection{Binary}
\todo[inline]{To be Designed}

%--------------------------------------------------------------------------------
\section{\xmsh}
%%-------------------------------------------------------------------------------
An XMSH File is considered valid if it satisfies the following conditions:
\begin{enumerate}
  \item The first section is a valid \fileinfo{}
  \item The second section is a valid \grid_info{}
  \item If the filetype (as specified in \fileinfo{}) is \inlinecode{serial},
    the rest of the file should satisfy \textit{Serial XMSH Requirements}.
  \item If the filetype (as specified in \fileinfo{}) is \inlinecode{parallel},
    the rest of the file should satisfy \textit{Parallel XMSH Requirements}.
\end{enumerate}


\subsection{Serial XMSH Requirements}

\begin{enumerate}
  \item It satisfies the \xmsp{} requirements.
  \item The \inlinecode{wdim} of \vertexlist{} is equal to \inlinecode{wdim} of
    \grid_info{}.
  \item For \vertexlist{}, $v_{i} < \text{nvertex}$
  \item For \elementlist{}, $e_{i} < \text{nelem}$
  \item For \datalist{}, $e_{i} < \text{nelem}$
  \item Value of \inlinecode{codim} for any of the \elementlist{} or \datalist{}
    sections is less than or equal to \inlinecode{dim} value of \grid_info{}.
  \item \inlinecode{nelem} for any \datalist{} section is equal to the
    \inlinecode{nelem} value of the \inlinecode{nelem} value of \elementlist{}
    section with identical \inlinecode{codim} value. The value is equal to
    \inlinecode{nvertex} in case the \inlinecode{codim} value is equal to
    \inlinecode{dim} value in \grid_info{}.
  \item For both \elementlist{} and \vertexlist{}, $v_{i,j} < \text{nvertex}$
    (as specified in \vertexlist{}).
\end{enumerate}

\subsection{Parallel XMSH Requirements}

\begin{enumerate}
  \item There is exactly one \patchlist{} section.
  \item Combine all the information from all \xmsp{} into one file using the
    rule that all sections which are characterized as equivalent are combined
    together. Then the resultant file should be a valid satisfy Serial XMSH
    Requirements.
\end{enumerate}


\section{Serial Example File}


\begin{lstlisting}
$fileinfo
version 1
filetype serial
label serial_demo
$endfileinfo

$grid_info
wdim 1
dim 1
$endgrid_info

$vertexlist
wdim 1
nvertex 5
0   -1.0
1   -0.5
2    0.0
3    0.5
4    1.0
$endvertexlist

$elementlist
codim 0
nelem 4
0 110 2 0 1
1 110 2 1 2
2 110 2 2 3
3 110 2 3 4
$endelementlist

$datalist
datatype integer
codim 0
label boundary_pair
nelem 4
0  1  3
1  1 -1
2  1 -1
3  1  0
$enddatalist

$datalist
datatype integer
codim 1
label boundary_pair
nelem 5
0  1  4
1  1 -1
2  1 -1
3  1 -1
4  1  0
$enddatalist
\end{lstlisting}


\section{Parallel Example File}

\begin{lstlisting}[caption=parfile.xmsh,captionpos=b]
$fileinfo
version 1
filetype serial
label parallel_demo
$endfileinfo

$grid_info
wdim 1
dim 1
$endgrid_info

$patchlist
npatch 10
0 ASCII parfile00.xmsp
1 ASCII parfile23.xmsp
$endpatchlist
\end{lstlisting}

\begin{lstlisting}[caption=parfile00.xmsp,captionpos=b]
$vertexlist
wdim 1
nvertex 3
0   -1.0
4    1.0
1   -0.5
$endvertexlist

$datalist
datatype integer
codim 1
label boundary_pair
nelem 5
0  1  4
1  1 -1
2  1 -1
3  1 -1
4  1  0
$enddatalist
\end{lstlisting}

\begin{lstlisting}[caption=parfile23.xmsp,captionpos=b]
$vertexlist
wdim 1
nvertex 2
2    0.0
3    0.5
$endvertexlist

$elementlist
codim 0
nelem 4
0 110 2 0 1
1 110 2 1 2
2 110 2 2 3
3 110 2 3 4
$endelementlist

$datalist
datatype integer
codim 0
label boundary_pair
nelem 4
0  1  3
1  1 -1
2  1 -1
3  1  0
$enddatalist
\end{lstlisting}









%################################################################################
\chapter{Reference Implementation}
%################################################################################






%\backmatter
%\printbibliography[heading=bibintoc]
%\printindex
\end{document}


