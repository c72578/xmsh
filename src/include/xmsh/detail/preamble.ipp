namespace xmsh::detail {

template <file_type::enum_type ft>
bool operator==(SectionInfo<ft> const& a, SectionInfo<ft> const& b)
{
  bool equality{true};
  equality = equality and (a.lineno == b.lineno);
  equality = equality and (a.section_type == b.section_type);
  return equality;
}

template <file_type::enum_type ft, typename A, typename B>
bool operator!=(SectionInfo<ft> const& a, SectionInfo<ft> const& b)
{
  return (not(a == b));
}

template <typename A>
preamble<file_type::ascii, A>::preamble(A const& a) : data(a)
{
}
template <typename A>
preamble<file_type::ascii, A>::preamble(preamble&& p, A const& a)
: data(std::move(p.data), a)
{
}

template <typename A, typename B>
bool operator==(preamble<file_type::ascii, A> const& a,
                preamble<file_type::ascii, B> const& b)
{
  return (a.data == b.data);
}

template <typename A, typename B>
bool operator!=(preamble<file_type::ascii, A> const& a,
                preamble<file_type::ascii, B> const& b)
{
  return (not(a == b));
}
} // namespace xmsh::detail
