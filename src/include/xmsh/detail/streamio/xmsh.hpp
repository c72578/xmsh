#ifndef XMSH_DETAIL_STREAMIO_XMSH_HPP
#define XMSH_DETAIL_STREAMIO_XMSH_HPP

#include <experimental/filesystem>
#include <iomanip>
#include <iostream>
#include <xmsh/detail/streamio/v1/xmsh.hpp>
#include <xmsh/detail/streamio/xmsh_info.hpp>
#include <xmsh/xmsh_data.hpp>

namespace xmsh::detail::streamio {
template <typename A>
auto read_xmsh(std::string_view const& filename, A const& a) -> xmsh_data<A>;

template <typename A>
void write(std::string_view const& filename,
           xmsh_info xmsh_info,
           xmsh_data<A> const& xmsh);

extern template auto read_xmsh(std::string_view const& filename,
                               std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

extern template void write(std::string_view const& filename,
                           xmsh_info xmsh_info,
                           xmsh_data<std::allocator<void>> const& xmsh);

} // namespace xmsh::detail::streamio

#include <xmsh/detail/streamio/xmsh.ipp>

#endif // XMSH_DETAIL_STREAMIO_XMSH_HPP
