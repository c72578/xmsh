namespace xmsh::detail::streamio {
template <typename A>
auto read_xmsh(std::string_view const& filename, A const& a) -> xmsh_data<A>
{
  auto const filename_str = std::string(filename);
  if (not std::experimental::filesystem::exists(filename)) {
    throw std::runtime_error("File Not Found.");
  }

  std::ifstream in(filename_str);

  auto const xmsh_info = read_xmsh_info(in, a);

  switch (xmsh_info.version) {
  case Version::one: {
    return xmsh::detail::streamio::v1::read_xmsh(in, xmsh_info, a);
  }
  default: {
    xmsh::detail::bug_log("Unexpected XMSH File Version");
    throw std::logic_error("Unexpected XMSH File Version");
  }
  }
}

template <typename A>
void write(std::string_view const& filename,
           xmsh_info xmsh_info,
           xmsh_data<A> const& xmsh)
{
  auto const filename_str = string<A>(filename);
  std::ofstream out(filename_str);
  out << std::setprecision(std::numeric_limits<double>::digits10 + 1);
  out << std::scientific;
  xmsh::detail::streamio::write(out, xmsh_info);
  out << "\n";
  switch (xmsh_info.version) {
  case Version::one: {
    xmsh::detail::streamio::v1::write(out, xmsh_info, xmsh);
    break;
  }
  default:
    xmsh::detail::bug_log("Unexpected Version");
    throw std::logic_error("Unexpected Version");
  }
}

} // namespace xmsh::detail::streamio
