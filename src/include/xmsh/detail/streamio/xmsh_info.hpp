#ifndef XMSH_DETAIL_STREAMIO_XMSH_INFO_HPP
#define XMSH_DETAIL_STREAMIO_XMSH_INFO_HPP

#include <xmsh/detail/system.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>
#include <xmsh/xmsh_info.hpp>

namespace xmsh::detail::streamio {
template <typename A>
auto read_xmsh_info(std::istream& in, A const& a) -> xmsh_info;

template <typename A = std::allocator<void>,
          typename = std::enable_if_t<std::is_default_constructible_v<A>>>
auto read_xmsh_info(std::istream& in) -> xmsh_info;

void write(std::ostream& out, xmsh_info const& xmsh_info);

extern template auto read_xmsh_info<std::allocator<void>>(std::istream& in)
  -> xmsh_info;

extern template auto read_xmsh_info(std::istream& in,
                                    std::allocator<void> const& a) -> xmsh_info;
} // namespace xmsh::detail::streamio

#include <xmsh/detail/streamio/xmsh_info.ipp>

#endif // XMSH_DETAIL_STREAMIO_XMSH_INFO_HPP
