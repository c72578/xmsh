namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_singlefile_xmsh(std::istream& in, A const& a) -> xmsh_data<A>
{
  xmsh_data<A> xmsh(a);
  xmsh.grid_info = xmsh::detail::streamio::v1::read_grid_info(in, a);
  auto const dim = xmsh.grid_info.dim.underlying_value();
  xmsh.codim_element_array.resize(dim);
  xmsh.float_grid_data.resize(dim + 1);
  xmsh.integer_grid_data.resize(dim + 1);

  auto read_status = SingleFileReadStatus<A>(dim + 1, a);
  while (!(in >> std::ws).eof()) {
    /// Store the Position of Section Header in Order to Come Back to It Later
    auto const position = in.tellg();

    /// Read the Section Header
    string<A> header_str(a);
    in >> header_str;

    /// Stop Reading File if Reading is Failing
    if (in.fail()) {
      break;
    };

    /// otherwise process section type
    auto const read_section_type
      = xmsh::detail::streamio::v1::header_to_section_type(header_str);

    switch (read_section_type) {
    case section_type::vertexarray: {
      if (read_status.vertex_array) {
        throw std::runtime_error("Multiple vertexarray section found.");
      }

      // Restore Back to Section Header Position
      in.seekg(position);
      xmsh.vertex_array = xmsh::detail::streamio::v1::read_vertex_array(in, a);
      read_status.vertex_array = true;
      break;
    }
    case section_type::elementarray: {
      string<A> codim_str(a);
      in >> codim_str;
      if (not xmsh::detail::validate(codim_str, "codim")) {
        throw std::runtime_error("Expected \"codim\".");
      }

      std::size_t codim;
      in >> codim;
      if (read_status.element_array[codim]) {
        throw std::runtime_error(
          "Multiple elementarray section found for one codim.");
      }

      in.seekg(position);
      xmsh.codim_element_array[codim]
        = xmsh::detail::streamio::v1::read_element_array(in, a);

      break;
    }
    case section_type::griddata: {
      auto const datatype = xmsh::read_key_string_pair<data_type>(in, a);
      string<A> codim_str(a);
      in >> codim_str;
      if (not xmsh::detail::validate(codim_str, "codim")) {
        throw std::runtime_error("Expected \"codim\".");
      }

      std::size_t codim;
      in >> codim;

      string<A> label_str(a);
      in >> label_str;
      if (not xmsh::detail::validate(label_str, "label")) {
        throw std::runtime_error("Expected \"label\".");
      }

      string<A> label(a);
      in >> label;

      in.seekg(position);

      switch (datatype) {
      case data_type::fp: {
        auto const it
          = std::find(std::begin(read_status.float_grid_data[codim]),
                      std::end(read_status.float_grid_data[codim]),
                      label);

        if (not(it == std::end(read_status.float_grid_data[codim]))) {
          throw std::runtime_error(
            "Multiple griddata found for this combination of codim and label.");
        }
        auto float_grid_data = read_grid_data<data_type::fp, A>(in, a);

        xmsh.float_grid_data[codim].insert(
          std::make_pair(label, std::move(float_grid_data)));
        break;
      }
      case data_type::integer: {
        auto const it
          = std::find(std::begin(read_status.integer_grid_data[codim]),
                      std::end(read_status.integer_grid_data[codim]),
                      label);

        if (not(it == std::end(read_status.integer_grid_data[codim]))) {
          throw std::runtime_error(
            "Multiple griddata found for this combination of codim and label.");
        }
        auto integer_grid_data
          = xmsh::detail::streamio::v1::read_grid_data<data_type::integer, A>(
            in, a);

        xmsh.integer_grid_data[codim].insert(
          std::make_pair(label, std::move(integer_grid_data)));
        break;
      }
      default:
        xmsh::detail::bug_log("Unexpected datatype");
        throw std::logic_error("Unexpected datatype");
      }

      break;
    }
    default:
      xmsh::detail::bug_log("Unexpected section_type");
      throw std::logic_error("Unexpected section_type");
    }
  }
  return std::move(xmsh);
}

template <typename A>
auto read_xmsh(std::istream& in, xmsh_info const& xmsh_info, A const& a)
  -> xmsh_data<A>
{
  switch (xmsh_info.is_multifile) {
  case multifile::no:
    return xmsh::detail::streamio::v1::read_singlefile_xmsh(in, a);
  case multifile::yes:
    throw std::runtime_error("Multifile Reader Not Yet Implemented");
  default:
    xmsh::detail::bug_log("Unexpected Multifile Type");
    throw std::logic_error("Unexpected Multifile Type");
  }
}
template <typename A>
void write_singlefile_xmsh(std::ostream& out, xmsh_data<A> const& xmsh)
{
  xmsh::detail::streamio::v1::write(out, xmsh.grid_info);
  out << "\n";
  xmsh::detail::streamio::v1::write(out, xmsh.vertex_array);
  out << "\n";
  for (auto const& el : xmsh.codim_element_array) {
    xmsh::detail::streamio::v1::write(out, el);
    out << "\n";
  }
  for (auto const& cfgd : xmsh.float_grid_data) {
    for (auto const& fgd : cfgd) {
      xmsh::detail::streamio::v1::write(out, fgd.second);
      out << "\n";
    }
  }
  for (auto const& cigd : xmsh.integer_grid_data) {
    for (auto const& igd : cigd) {
      xmsh::detail::streamio::v1::write(out, igd.second);
      out << "\n";
    }
  }
}

template <typename A>
void write(std::ostream& out,
           xmsh_info const& xmsh_info,
           xmsh_data<A> const& xmsh)
{
  switch (xmsh_info.is_multifile) {
  case multifile::no:
    xmsh::detail::streamio::v1::write_singlefile_xmsh(out, xmsh);
    break;
  case multifile::yes:
    xmsh::detail::bug_log("Unexpected Multifile Type");
    throw std::runtime_error("Multifile Reader Not Yet Implemented");
  default:
    xmsh::detail::bug_log("Unexpected Multifile Type");
    throw std::logic_error("Unexpected Multifile Type");
  }
}
} // namespace xmsh::detail::streamio::v1
