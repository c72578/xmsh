namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_preamble(std::istream& in, A const& a)
  -> preamble<file_type::ascii, A>
{
  constexpr auto ft = file_type::ascii;
  preamble<ft, A> preamble(a);

  string<A> input(a);

  in >> input;
  if (not xmsh::detail::validate(input, "$preamble")) {
    throw std::runtime_error("Unexpected Header");
  }

  in >> input;
  if (not xmsh::detail::validate(input, "nsection")) {
    throw std::runtime_error("Expected \"nsection\"");
  }

  std::size_t nsection;
  in >> nsection;

  for (std::size_t section = 0; section < nsection; ++section) {
    std::size_t index;
    in >> index;
    if (section != index) {
      throw std::runtime_error("Inconsistent Section Index");
    }
    std::size_t lineno;
    in >> input;
    auto const read_section_type = xmsh::to_enumeration<section_type>(input);
    in >> lineno;
    auto sectioninfo = SectionInfo<file_type::ascii>();
    sectioninfo.lineno = lineno;
    sectioninfo.section_type = read_section_type;
    preamble.data.emplace_back(sectioninfo);
  }

  in >> input;
  if (not xmsh::detail::validate(input, "$endpreamble")) {
    throw std::runtime_error("Unexpected Footer");
  }
  return preamble;
}

template <typename A>
void write(std::ostream& out, preamble<file_type::ascii, A> const& preamble)
{
  auto const& data = preamble.data;
  out << "$preamble"
      << "\n";
  auto const nsection = data.size();
  out << "nsection " << nsection << "\n";
  for (std::size_t section = 0; section < nsection; ++section) {
    out << section << " " << xmsh::to_string_view(data[section].section_type)
        << " " << data[section].lineno << "\n";
  }
  out << "$endpreamble"
      << "\n";
}

} // namespace xmsh::detail::streamio::v1
