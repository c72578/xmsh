#ifndef XMSH_DETAIL_STREAMIO_V1_TO_SECTION_TYPE_HPP
#define XMSH_DETAIL_STREAMIO_V1_TO_SECTION_TYPE_HPP

#include <xmsh/detail/streamio/v1/traits.hpp>
#include <xmsh/error_code.hpp>

namespace xmsh::detail::streamio::v1 {

auto header_to_section_type(std::string_view const& header) -> section_type;

} // namespace xmsh::detail::streamio::v1

#endif // XMSH_DETAIL_STREAMIO_V1_TO_SECTION_TYPE_HPP
