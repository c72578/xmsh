#ifndef XMSH_DETAIL_STREAMIO_V1_VERTEX_ARRAY_HPP
#define XMSH_DETAIL_STREAMIO_V1_VERTEX_ARRAY_HPP

#include <iostream>
#include <xmsh/detail/system.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>
#include <xmsh/vertex_array.hpp>

namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_vertex_array(std::istream& in, A const& a) -> vertex_array<A>;

template <typename A>
void write(std::ostream& out, vertex_array<A> const& vertex_array);

extern template auto read_vertex_array(std::istream& in,
                                       std::allocator<void> const& a)
  -> vertex_array<std::allocator<void>>;

extern template void write(
  std::ostream& out, vertex_array<std::allocator<void>> const& vertex_array);

} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/vertex_array.ipp>

#endif // XMSH_DETAIL_STREAMIO_V1_VERTEX_ARRAY_HPP
