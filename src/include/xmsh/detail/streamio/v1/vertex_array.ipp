namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_vertex_array(std::istream& in, A const& a) -> vertex_array<A>
{
  vertex_array<A> vertex_array(a);

  string<A> input(a);
  in >> input;
  if (not xmsh::detail::validate(input, "$vertexarray")) {
    throw std::runtime_error("Unexpected Header");
  }

  string<A> nvertex_string;
  in >> nvertex_string;
  if (not xmsh::detail::validate(nvertex_string, "nvertex")) {
    throw std::runtime_error("Expected \"nvertex\".");
  }
  std::size_t nvertex;
  in >> nvertex;

  string<A> wdim_string(a);
  in >> wdim_string;
  if (not xmsh::detail::validate(wdim_string, "wdim")) {
    throw std::runtime_error("Expected \"wdim\".");
  }
  in >> vertex_array.wdim;

  vertex_array.data.resize(nvertex);

  for (std::size_t i = 0; i < nvertex; ++i) {
    in >> vertex_array.data[i].index;
    vertex_array.data[i].coord.resize(vertex_array.wdim);
    for (std::size_t j = 0; j < vertex_array.wdim; ++j) {
      in >> vertex_array.data[i].coord[j];
    }
  }

  in >> input;
  if (not xmsh::detail::validate(input, "$endvertexarray")) {
    throw std::runtime_error("Unexpected Footer");
  }

  return vertex_array;
}

template <typename A>
void write(std::ostream& out, vertex_array<A> const& vertex_array)
{
  out << "$vertexarray\n";
  auto const size = vertex_array.data.size();
  out << "nvertex " << size << "\n";
  out << "wdim " << vertex_array.wdim << "\n";
  for (std::size_t i = 0; i < size; ++i) {
    out << vertex_array.data[i].index;
    auto const ncomp = vertex_array.data[i].coord.size();
    for (std::size_t j = 0; j < ncomp; ++j) {
      out << " " << vertex_array.data[i].coord[j];
    }
    out << "\n";
  }
  out << "$endvertexarray"
      << "\n";
}
} // namespace xmsh::detail::streamio::v1
