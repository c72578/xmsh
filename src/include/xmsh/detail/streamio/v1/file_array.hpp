#ifndef XMSH_DETAIL_STREAMIO_V1_FILE_ARRAY_HPP
#define XMSH_DETAIL_STREAMIO_V1_FILE_ARRAY_HPP

#include <iostream>
#include <xmsh/detail/system.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>
#include <xmsh/file_array.hpp>

namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_file_array(std::istream& in, A const& a) -> file_array<A>;

template <typename A>
void write(std::ostream& out, file_array<A> const& filearray);

extern template auto read_file_array(std::istream& in,
                                     std::allocator<void> const& a)
  -> file_array<std::allocator<void>>;

extern template void write(std::ostream& out,
                           file_array<std::allocator<void>> const& filearray);
} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/file_array.ipp>

#endif // XMSH_DETAIL_STREAMIO_V1_FILE_ARRAY_HPP
