#ifndef XMSH_DETAIL_STREAMIO_V1_TRAITS_HPP
#define XMSH_DETAIL_STREAMIO_V1_TRAITS_HPP
#include <xmsh/section_type.hpp>

namespace xmsh::detail::streamio::v1 {
template <xmsh::section_type::enum_type sectiontype>
struct SectionTraits;

template <>
struct SectionTraits<section_type::vertexarray> {
  static constexpr std::string_view header = "$vertexarray";
  static constexpr std::string_view footer = "$endvertexarray";
};

template <>
struct SectionTraits<section_type::elementarray> {
  static constexpr std::string_view header = "$elementarray";
  static constexpr std::string_view footer = "$endelementarray";
};

template <>
struct SectionTraits<section_type::griddata> {
  static constexpr std::string_view header = "$griddata";
  static constexpr std::string_view footer = "$endgriddata";
};
} // namespace xmsh::detail::streamio::v1

#endif // XMSH_DETAIL_STREAMIO_V1_TRAITS_HPP
