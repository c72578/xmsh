namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_element_array(std::istream& in, A const& a) -> element_array<A>
{
  string<A> input(a);
  in >> input;
  if (not xmsh::detail::validate(input, "$elementarray")) {
    throw std::runtime_error("Unexpected Header");
  }

  element_array<A> element_array(a);
  string<A> codim_string(a);
  in >> codim_string;
  if (not xmsh::detail::validate(codim_string, "codim")) {
    throw std::runtime_error("Expected \"codim\".");
  }
  in >> element_array.codim;

  string<A> nelem_string;
  in >> nelem_string;
  if (not xmsh::detail::validate(nelem_string, "nelem")) {
    throw std::runtime_error("Expected \"nelem\".");
  }
  std::size_t nelem;
  in >> nelem;

  element_array.data.resize(nelem);
  for (std::size_t i = 0; i < nelem; ++i) {
    in >> element_array.data[i].index;

    std::size_t element_type_uv;
    in >> element_type_uv;

    element_array.data[i].type
      = xmsh::to_enumeration<element_type>(element_type_uv);

    std::size_t npoint;
    in >> npoint;
    element_array.data[i].vertex_array.resize(npoint);
    for (std::size_t j = 0; j < npoint; ++j) {
      in >> element_array.data[i].vertex_array[j];
    }
  }

  in >> input;
  if (not xmsh::detail::validate(input, "$endelementarray")) {
    throw std::runtime_error("Unexpected Footer");
  }

  return element_array;
}

template <typename A>
void write(std::ostream& out, element_array<A> const& element_array)
{
  out << "$elementarray\n";
  out << "codim " << element_array.codim << "\n";
  out << "nelem " << element_array.data.size() << "\n";
  auto const nelem = element_array.data.size();

  auto const& data = element_array.data;
  for (std::size_t i = 0; i < nelem; ++i) {
    auto const nvertex = data[i].vertex_array.size();
    out << data[i].index << " " << data[i].type.underlying_value() << "  "
        << nvertex;
    for (std::size_t j = 0; j < nvertex; ++j) {
      out << " " << data[i].vertex_array[j];
    }
    out << "\n";
  }
  out << "$endelementarray\n";
}

} // namespace xmsh::detail::streamio::v1
