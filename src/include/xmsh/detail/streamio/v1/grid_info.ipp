namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_grid_info(std::istream& in, A const& a) -> grid_info<A>
{
  grid_info<A> grid_info(a);

  string<A> input(a);

  in >> input;
  if (not xmsh::detail::validate(input, "$gridinfo")) {
    throw std::runtime_error("Unexpected Header");
  }

  in >> input;
  if (not xmsh::detail::validate(input, "label")) {
    throw std::runtime_error("Expected \"label\".");
  }
  in >> grid_info.label;

  grid_info.wdim = xmsh::read_key_value_pair<world_dimension>(in, a);
  grid_info.dim = xmsh::read_key_value_pair<grid_dimension>(in, a);

  in >> input;
  if (not xmsh::detail::validate(input, "$endgridinfo")) {
    throw std::runtime_error("Unexpected Footer");
  }

  return grid_info;
}

template <typename A>
void write(std::ostream& out, grid_info<A> const& grid_info)
{
  out << "$gridinfo"
      << "\n";
  out << "label " << grid_info.label << "\n";
  xmsh::write_key_value_pair(out, grid_info.wdim);
  xmsh::write_key_value_pair(out, grid_info.dim);
  out << "$endgridinfo"
      << "\n";
}

} // namespace xmsh::detail::streamio::v1
