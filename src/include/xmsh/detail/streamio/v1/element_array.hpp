#ifndef XMSH_DETAIL_STREAMIO_V1_ELEMENT_ARRAY_HPP
#define XMSH_DETAIL_STREAMIO_V1_ELEMENT_ARRAY_HPP

#include <iostream>
#include <xmsh/detail/system.hpp>
#include <xmsh/element_array.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>

namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_element_array(std::istream& in, A const& a) -> element_array<A>;

template <typename A>
void write(std::ostream& out, element_array<A> const& element_array);

extern template auto read_element_array(std::istream& in,
                                        std::allocator<void> const& a)
  -> element_array<std::allocator<void>>;

extern template void write(
  std::ostream& out, element_array<std::allocator<void>> const& element_array);

} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/element_array.ipp>
#endif // XMSH_DETAIL_STREAMIO_V1_ELEMENT_ARRAY_HPP
