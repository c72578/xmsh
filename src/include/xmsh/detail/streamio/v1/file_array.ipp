namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_file_array(std::istream& in, A const& a) -> file_array<A>
{
  file_array<A> filearray(a);

  string<A> input(a);

  in >> input;
  if (not xmsh::detail::validate(input, "$filearray")) {
    throw std::runtime_error("Unexpected Header");
  }

  in >> input;
  std::size_t nfile;
  if (not xmsh::detail::validate(input, "nfile")) {
    throw std::runtime_error("Expected \"nfile\"");
  }
  in >> nfile;

  filearray.data.resize(nfile);

  for (std::size_t file_index = 0; file_index < nfile; ++file_index) {
    in >> filearray.data[file_index].index;
    in >> input;
    filearray.data[file_index].file_type
      = xmsh::to_enumeration<file_type>(input);
    in >> filearray.data[file_index].filename;
  }

  in >> input;

  if (not xmsh::detail::validate(input, "$endfilearray")) {
    throw std::runtime_error("Unexpected Footer");
  }

  return filearray;
}

template <typename A>
void write(std::ostream& out, file_array<A> const& filearray)
{
  auto const& data = filearray.data;
  out << "$filearray"
      << "\n";
  out << "nfile " << data.size() << "\n";
  for (std::size_t index = 0; index < data.size(); ++index) {
    out << data[index].index << " "
        << xmsh::to_string_view(data[index].file_type) << " "
        << data[index].filename << "\n";
  }
  out << "$endfilearray"
      << "\n";
}
} // namespace xmsh::detail::streamio::v1
