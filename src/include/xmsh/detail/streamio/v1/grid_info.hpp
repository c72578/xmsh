#ifndef XMSH_DETAIL_STREAMIO_V1_GRID_INFO_HPP
#define XMSH_DETAIL_STREAMIO_V1_GRID_INFO_HPP

#include <xmsh/detail/system.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>
#include <xmsh/grid_info.hpp>

namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_grid_info(std::istream& in, A const& a) -> grid_info<A>;

template <typename A>
void write(std::ostream& out, grid_info<A> const& grid_info);

extern template auto read_grid_info(std::istream& in,
                                    std::allocator<void> const& a)
  -> grid_info<std::allocator<void>>;

extern template void write(std::ostream& out,
                           grid_info<std::allocator<void>> const& grid_info);

} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/grid_info.ipp>

#endif // XMSH_DETAIL_STREAMIO_V1_GRID_INFO_HPP
