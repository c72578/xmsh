#ifndef XMSH_DETAIL_STREAMIO_V1_PREAMBLE_HPP
#define XMSH_DETAIL_STREAMIO_V1_PREAMBLE_HPP

#include <iostream>
#include <xmsh/detail/preamble.hpp>
#include <xmsh/detail/streamio/v1/to_section_type.hpp>
#include <xmsh/detail/system.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>

namespace xmsh::detail::streamio::v1 {
template <typename A>
auto read_preamble(std::istream& in, A const& a)
  -> preamble<file_type::ascii, A>;

template <typename A>
void write(std::ostream& out, preamble<file_type::ascii, A> const& preamble);
} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/preamble.ipp>

#endif // XMSH_DETAIL_STREAMIO_V1_PREAMBLE_HPP
