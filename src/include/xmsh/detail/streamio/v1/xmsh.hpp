#ifndef XMSH_DETAIL_STREAMIO_V1_XMSH_HPP
#define XMSH_DETAIL_STREAMIO_V1_XMSH_HPP

#include <xmsh/detail/streamio/v1/element_array.hpp>
#include <xmsh/detail/streamio/v1/grid_data.hpp>
#include <xmsh/detail/streamio/v1/grid_info.hpp>
#include <xmsh/detail/streamio/v1/to_section_type.hpp>
#include <xmsh/detail/streamio/v1/vertex_array.hpp>
#include <xmsh/section_type.hpp>
#include <xmsh/xmsh_data.hpp>
#include <xmsh/xmsh_info.hpp>

namespace xmsh::detail::streamio::v1 {
template <typename A>
struct SingleFileReadStatus {
  bool vertex_array{false};
  vector<bool, A> element_array;
  vector<vector<string<A>, A>, A> float_grid_data;
  vector<vector<string<A>, A>, A> integer_grid_data;

  explicit SingleFileReadStatus(std::size_t dim, A const& a)
  : element_array(a), float_grid_data(a), integer_grid_data(a)
  {
    element_array.resize(dim);
    float_grid_data.resize(dim);
    integer_grid_data.resize(dim);
  }
};

template <typename A>
auto read_singlefile_xmsh(std::istream& in, A const& a) -> xmsh_data<A>;

template <typename A>
void write_singlefile_xmsh(std::ostream& out, xmsh_data<A> const& xmsh);

template <typename A>
auto read_xmsh(std::istream& in, xmsh_info const& xmsh_info, A const& a)
  -> xmsh_data<A>;

template <typename A>
void write(std::ostream& out,
           xmsh_info const& xmsh_info,
           xmsh_data<A> const& xmsh);

extern template struct SingleFileReadStatus<std::allocator<void>>;

extern template auto read_singlefile_xmsh(std::istream& in,
                                          std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

extern template void write_singlefile_xmsh(
  std::ostream& out, xmsh_data<std::allocator<void>> const& xmsh);

extern template auto read_xmsh(std::istream& in,
                               xmsh_info const& xmsh_info,
                               std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

extern template void write(std::ostream& filename,
                           xmsh_info const& xmsh_info,
                           xmsh_data<std::allocator<void>> const& xmsh);

} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/xmsh.ipp>

#endif // XMSH_DETAIL_STREAMIO_V1_XMSH_HPP
