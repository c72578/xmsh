#ifndef XMSH_DETAIL_STREAMIO_V1_GRID_DATA_HPP
#define XMSH_DETAIL_STREAMIO_V1_GRID_DATA_HPP

#include <iostream>
#include <xmsh/detail/system.hpp>
#include <xmsh/enumeration.hpp>
#include <xmsh/error_code.hpp>
#include <xmsh/grid_data.hpp>
#include <xmsh/meta/buildinfo.hpp>

namespace xmsh::detail::streamio::v1 {
template <data_type::enum_type datatype, typename A>
auto read_grid_data(std::istream& in, A const& a) -> grid_data<datatype, A>;

template <data_type::enum_type datatype, typename A>
void write(std::ostream& out, grid_data<datatype, A> const& grid_data);

extern template auto read_grid_data(std::istream& in,
                                    std::allocator<void> const& a)
  -> grid_data<data_type::fp, std::allocator<void>>;

extern template void write(
  std::ostream& out,
  grid_data<data_type::fp, std::allocator<void>> const& grid_data);

extern template auto read_grid_data(std::istream& in,
                                    std::allocator<void> const& a)
  -> grid_data<data_type::integer, std::allocator<void>>;

extern template void write(
  std::ostream& out,
  grid_data<data_type::integer, std::allocator<void>> const& grid_data);

} // namespace xmsh::detail::streamio::v1

#include <xmsh/detail/streamio/v1/grid_data.ipp>
#endif // XMSH_DETAIL_STREAMIO_V1_GRID_DATA_HPP
