namespace xmsh::detail::streamio::v1 {
template <data_type::enum_type datatype, typename A>
auto read_grid_data(std::istream& in, A const& a) -> grid_data<datatype, A>
{

  grid_data<datatype, A> gd(a);

  string<A> header_str(a);
  in >> header_str;
  if (not xmsh::detail::validate(header_str, "$griddata")) {
    throw std::runtime_error("Unexpected Header");
  }

  auto const read_data_type = read_key_string_pair<data_type>(in, a);
  if (read_data_type != datatype) {
    throw std::logic_error("Inconsistent data_type encountered");
  }

  string<A> codim_str(a);
  in >> codim_str;
  if (not xmsh::detail::validate(codim_str, "codim")) {
    throw std::runtime_error("Expected \"codim\".");
  }
  in >> gd.codim;

  string<A> label_str(a);
  in >> label_str;
  if (not xmsh::detail::validate(label_str, "label")) {
    throw std::runtime_error("Expected \"label\".");
  }
  in >> gd.label;

  string<A> nelem_str(a);
  in >> nelem_str;
  if (not xmsh::detail::validate(nelem_str, "nelem")) {
    throw std::runtime_error("Expected \"nelem\".");
  }
  std::size_t nelem;
  in >> nelem;

  gd.data.resize(nelem);
  for (std::size_t i = 0; i < nelem; ++i) {
    in >> gd.data[i].index;
    std::size_t nvalue;
    in >> nvalue;

    gd.data[i].value.resize(nvalue);
    for (std::size_t j = 0; j < nvalue; ++j) {
      in >> gd.data[i].value[j];
    }
  }

  string<A> closing_header_str(a);
  in >> closing_header_str;
  if (not xmsh::detail::validate(closing_header_str, "$endgriddata")) {
    throw std::runtime_error("Unexpected Footer");
  }

  return gd;
}

template <data_type::enum_type datatype, typename A>
void write(std::ostream& out, grid_data<datatype, A> const& grid_data)
{
  out << "$griddata\n";
  xmsh::write_key_string_pair(out, grid_data.dt);
  out << "codim " << grid_data.codim << "\n";
  out << "label " << grid_data.label << "\n";
  out << "nelem " << grid_data.data.size() << "\n";
  auto const nelem = grid_data.data.size();

  if constexpr (datatype == data_type::fp) {
    out.precision(std::numeric_limits<double>::max_digits10);
    out << std::scientific;
  }

  auto const& data = grid_data.data;
  for (std::size_t i = 0; i < nelem; ++i) {
    auto const nvalue = data[i].value.size();
    out << data[i].index << "  " << nvalue;
    for (std::size_t j = 0; j < nvalue; ++j) {
      out << " " << data[i].value[j];
    }
    out << "\n";
  }
  out << "$endgriddata\n";
}

} // namespace xmsh::detail::streamio::v1
