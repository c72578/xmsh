namespace xmsh::detail::streamio {
template <typename A, typename T>
auto read_xmsh_info(std::istream& in) -> xmsh_info
{
  return read_xmsh_info(in, A());
}

template <typename A>
auto read_xmsh_info(std::istream& in, A const& a) -> xmsh_info
{
  xmsh_info xmsh;

  string<A> input(a);

  in >> input;
  if (not xmsh::detail::validate(input, "$xmshinfo")) {
    throw std::runtime_error("Expected Header");
  }

  xmsh.version = read_key_value_pair<Version>(in, a);
  xmsh.is_multifile = read_key_string_pair<multifile>(in, a);

  in >> input;
  if (not xmsh::detail::validate(input, "$endxmshinfo")) {
    throw std::runtime_error("Expected Footer");
  }

  return xmsh;
}

} // namespace xmsh::detail::streamio
