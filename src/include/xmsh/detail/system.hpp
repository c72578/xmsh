#ifndef XMSH_DETAIL_SYSTEM_HPP
#define XMSH_DETAIL_SYSTEM_HPP

#include <string_view>

namespace xmsh::detail {
bool validate(std::string_view s1, std::string_view s2);
} // namespace xmsh::detail

#endif // XMSH_DETAIL_SYSTEM_HPP
