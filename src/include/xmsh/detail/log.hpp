#ifndef XMSH_DETAIL_LOG_HPP
#define XMSH_DETAIL_LOG_HPP

#include <iostream>
#include <string_view>
namespace xmsh::detail {
void debug_log(std::string_view message);
void bug_log(std::string_view message);
} // namespace xmsh::detail

#endif // XMSH_DETAIL_LOG_HPP
