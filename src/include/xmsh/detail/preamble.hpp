#ifndef XMSH_DETAIL_PREAMBLE_HPP
#define XMSH_DETAIL_PREAMBLE_HPP

#include <xmsh/base.hpp>
#include <xmsh/file_type.hpp>
#include <xmsh/section_type.hpp>

namespace xmsh::detail {
template <file_type::enum_type ft, typename A = std::allocator<void>>
struct preamble;

template <file_type::enum_type ft>
struct SectionInfo;

template <>
struct SectionInfo<file_type::ascii> {
  std::size_t lineno{max_size};
  struct section_type section_type {
    section_type::vertexarray
  };
};

template <file_type::enum_type ft>
bool operator==(SectionInfo<ft> const& a, SectionInfo<ft> const& b);
template <file_type::enum_type ft, typename A, typename B>
bool operator!=(SectionInfo<ft> const& a, SectionInfo<ft> const& b);

template <typename A>
struct preamble<file_type::ascii, A> {
  static constexpr auto filetype = file_type::ascii;
  using allocator_type = A;

  vector<SectionInfo<filetype>, A> data;

  preamble() = default;
  explicit preamble(A const& a);
  preamble(preamble&& p, A const& a);
};

template <file_type::enum_type ft, typename A, typename B>
bool operator==(preamble<ft, A> const& a, preamble<ft, B> const& b);
template <file_type::enum_type ft, typename A, typename B>
bool operator!=(preamble<ft, A> const& a, preamble<ft, B> const& b);
} // namespace xmsh::detail

#include <xmsh/detail/preamble.ipp>

#endif // XMSH_DETAIL_PREAMBLE_HPP
