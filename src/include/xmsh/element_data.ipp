namespace xmsh {
template <data_type::enum_type datatype, typename A>
element_data<datatype, A>::element_data(A const& a) : value(a)
{
}

template <data_type::enum_type datatype, typename A, typename B>
bool operator==(element_data<datatype, A> const& a,
                element_data<datatype, B> const& b)
{
  using type = typename Scalar<datatype>::type;
  constexpr auto epsilon = std::numeric_limits<type>::epsilon();

  bool equality{true};

  equality = equality and (a.index == b.index);

  auto const nvalue_a = a.value.size();
  auto const nvalue_b = b.value.size();
  equality = equality and (nvalue_a == nvalue_b);

  if (equality) {
    for (std::size_t i = 0; i < nvalue_a; ++i) {
      equality = equality and (std::abs(a.value[i] - b.value[i]) <= epsilon);
    }
  }
  return equality;
}

template <data_type::enum_type datatype, typename A, typename B>
bool operator!=(element_data<datatype, A> const& a,
                element_data<datatype, B> const& b)
{
  return (not(a == b));
}

} // namespace xmsh
