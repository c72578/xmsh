#ifndef XMSH_BASE_HPP
#define XMSH_BASE_HPP

#include <array>
#include <map>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

namespace xmsh::detail {
template <class T, class A>
using rebind_to = typename std::allocator_traits<A>::template rebind_alloc<T>;
constexpr inline std::size_t max_size = std::numeric_limits<std::size_t>::max();
} // namespace xmsh::detail

namespace xmsh {
template <class T, class U, class A>
using map
  = std::map<T, U, std::less<T>, detail::rebind_to<std::pair<T const, U>, A>>;

template <class A>
using string
  = std::basic_string<char, std::char_traits<char>, detail::rebind_to<char, A>>;

template <class T, class A>
using vector = std::vector<T, detail::rebind_to<T, A>>;

template <typename T>
struct is_enumeration : std::false_type {
};

template <typename... Ts>
constexpr auto make_array(Ts&&... ts)
  -> std::array<std::common_type_t<Ts...>, sizeof...(ts)>
{
  return {std::forward<Ts>(ts)...};
}
} // namespace xmsh

#endif // XMSH_BASE_HPP
