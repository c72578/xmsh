#ifndef XMSH_XMSH_DATA_HPP
#define XMSH_XMSH_DATA_HPP

#include <fstream>
#include <xmsh/data_type.hpp>
#include <xmsh/element_array.hpp>
#include <xmsh/grid_data.hpp>
#include <xmsh/grid_info.hpp>
#include <xmsh/vertex_array.hpp>

namespace xmsh {

template <typename A = std::allocator<void>>
struct xmsh_data {
  using allocator_type = A;

  template <data_type::enum_type dt>
  using codim_grid_data = map<string<A>, grid_data<dt, A>, A>;

  struct grid_info<A> grid_info;
  struct vertex_array<A> vertex_array;
  vector<element_array<A>, A> codim_element_array;
  vector<codim_grid_data<data_type::fp>, A> float_grid_data;
  vector<codim_grid_data<data_type::integer>, A> integer_grid_data;

  xmsh_data() = default;
  explicit xmsh_data(A const& a);

  xmsh_data(xmsh_data const&) = delete;
  xmsh_data& operator=(xmsh_data const&) = delete;

  xmsh_data(xmsh_data&& xmsh) noexcept = default;
  xmsh_data& operator=(xmsh_data&& xmsh) noexcept = default;
  xmsh_data(xmsh_data&& xmsh, A const& a);
  ~xmsh_data() = default;
};

template <typename A, typename B>
bool operator==(xmsh_data<A> const& a, xmsh_data<B> const& b);

template <typename A, typename B>
bool operator!=(xmsh_data<A> const& a, xmsh_data<B> const& b);

extern template bool operator==(xmsh_data<std::allocator<void>> const& a,
                                xmsh_data<std::allocator<void>> const& b);
extern template bool operator!=(xmsh_data<std::allocator<void>> const& a,
                                xmsh_data<std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/xmsh_data.ipp>

#endif // XMSH_XMSH_DATA_HPP
