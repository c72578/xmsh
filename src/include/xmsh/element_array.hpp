#ifndef XMSH_ELEMENT_ARRAY_HPP
#define XMSH_ELEMENT_ARRAY_HPP

#include <algorithm>
#include <vector>
#include <xmsh/element_type.hpp>

namespace xmsh {
template <typename A = std::allocator<void>>
struct element {
  using allocator_type = A;

  element() = default;
  explicit element(A const& a);

  std::size_t index{detail::max_size};
  element_type type{element_type::vertex};
  std::vector<std::size_t, detail::rebind_to<std::size_t, A>> vertex_array;
};

template <typename A, typename B>
bool operator==(element<A> const& a, element<B> const& b);

template <typename A, typename B>
bool operator!=(element<A> const& a, element<B> const& b);

extern template struct element<std::allocator<void>>;
extern template bool operator==(element<std::allocator<void>> const& a,
                                element<std::allocator<void>> const& b);
extern template bool operator!=(element<std::allocator<void>> const& a,
                                element<std::allocator<void>> const& b);

template <typename A = std::allocator<void>>
struct element_array {
  using allocator_type = A;

  std::size_t codim{1};
  std::vector<element<A>, detail::rebind_to<element<A>, A>> data;

  element_array() = default;
  explicit element_array(A const& a);

  element_array(element_array const&) = delete;
  element_array& operator=(element_array const&) = delete;

  element_array(element_array&& ea) noexcept = default;
  element_array& operator=(element_array&& ea) noexcept = default;
  element_array(element_array&& ea, A const& a);
  ~element_array() = default;
};

template <typename A, typename B>
bool operator==(element_array<A> const& a, element_array<B> const& b);

template <typename A, typename B>
bool operator!=(element_array<A> const& a, element_array<B> const& b);

extern template struct element_array<std::allocator<void>>;
extern template bool operator==(element_array<std::allocator<void>> const& a,
                                element_array<std::allocator<void>> const& b);
extern template bool operator!=(element_array<std::allocator<void>> const& a,
                                element_array<std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/element_array.ipp>
#endif // XMSH_ELEMENT_ARRAY_HPP
