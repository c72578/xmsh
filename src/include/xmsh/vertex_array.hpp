#ifndef XMSH_VERTEX_ARRAY_HPP
#define XMSH_VERTEX_ARRAY_HPP

#include <algorithm>
#include <cmath>
#include <iostream>
#include <optional>
#include <vector>
#include <xmsh/base.hpp>
#include <xmsh/detail/system.hpp>

namespace xmsh {

template <typename A = std::allocator<void>>
struct Vertex {
  using allocator_type = A;

  std::size_t index{detail::max_size};
  std::vector<double, detail::rebind_to<double, A>> coord;

  Vertex() = default;
  explicit Vertex(A const& a);
  Vertex(Vertex&& vertex, A const& a);
};

template <typename A, typename B>
bool operator==(Vertex<A> const& a, Vertex<B> const& b);
template <typename A, typename B>
bool operator!=(Vertex<A> const& a, Vertex<B> const& b);

extern template struct Vertex<std::allocator<void>>;
extern template bool operator==(Vertex<std::allocator<void>> const& a,
                                Vertex<std::allocator<void>> const& b);
extern template bool operator!=(Vertex<std::allocator<void>> const& a,
                                Vertex<std::allocator<void>> const& b);

template <typename A = std::allocator<void>>
struct vertex_array {
  using allocator_type = A;

  std::size_t wdim{1};
  std::vector<Vertex<A>, detail::rebind_to<Vertex<A>, A>> data;

  vertex_array() = default;
  explicit vertex_array(A const& a);

  vertex_array(vertex_array&&) noexcept = default;
  vertex_array& operator=(vertex_array&&) noexcept = default;
  vertex_array(vertex_array&& vertex_array, A const& a);
  ~vertex_array() = default;

  vertex_array(vertex_array const&) = delete;
  vertex_array& operator=(vertex_array const&) = delete;
};

template <typename A, typename B>
bool operator==(vertex_array<A> const& a, vertex_array<B> const& b);
template <typename A, typename B>
bool operator!=(vertex_array<A> const& a, vertex_array<B> const& b);

extern template struct vertex_array<std::allocator<void>>;
extern template bool operator==(vertex_array<std::allocator<void>> const& a,
                                vertex_array<std::allocator<void>> const& b);
extern template bool operator!=(vertex_array<std::allocator<void>> const& a,
                                vertex_array<std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/vertex_array.ipp>

#endif // XMSH_VERTEX_ARRAY_HPP
