namespace xmsh {
template <typename A>
file_info<A>::file_info(A const& a) : filename(a)
{
}

template <typename A>
file_info<A>::file_info(file_info const& fi, A const& a)
: filename(fi.filename, a), file_type(fi.file_type)
{
}

template <typename A>
file_info<A>::file_info(file_info&& fi, A const& a)
: filename(std::move(fi.filename), a), file_type(fi.file_type)
{
}

template <typename A, typename B>
bool operator==(file_info<A> const& a, file_info<B> const& b)
{
  bool equality{true};
  equality = equality and (a.index == b.index);
  equality = equality and (a.filename == b.filename);
  equality = equality and (a.file_type == b.file_type);
  return equality;
}

template <typename A, typename B>
bool operator!=(file_info<A> const& a, file_info<B> const& b)
{
  return (not(a == b));
}

template <typename A>
file_array<A>::file_array(A const& a) : data(a)
{
}

template <typename A>
file_array<A>::file_array(file_array const& fia, A const& a) : data(fia.data, a)
{
}

template <typename A>
file_array<A>::file_array(file_array&& fia, A const& a)
: data(std::move(fia.data), a)
{
}

template <typename A, typename B>
bool operator==(file_array<A> const& a, file_array<B> const& b)
{
  return a.data == b.data;
}

template <typename A, typename B>
bool operator!=(file_array<A> const& a, file_array<B> const& b)
{
  return (not(a == b));
}

} // namespace xmsh
