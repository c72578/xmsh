#ifndef XMSH_GRID_DATA_HPP
#define XMSH_GRID_DATA_HPP

#include <xmsh/element_data.hpp>
#include <xmsh/enumeration.hpp>

namespace xmsh {

template <data_type::enum_type datatype, typename A = std::allocator<void>>
struct grid_data {
  using allocator_type = A;
  data_type dt = to_enumeration<data_type>(datatype);
  using Data = element_data<datatype>;
  std::size_t codim{detail::max_size};
  string<A> label;
  vector<Data, A> data;

  grid_data() = default;
  explicit grid_data(A const& a);

  grid_data(grid_data const&) = delete;
  grid_data& operator=(grid_data const&) = delete;

  grid_data(grid_data&&) noexcept = default;
  grid_data& operator=(grid_data&&) noexcept = default;
  grid_data(grid_data&& dl, A const& a);
  ~grid_data() = default;
};

template <data_type::enum_type datatype, typename A, typename B>
bool operator==(grid_data<datatype, A> const& a,
                grid_data<datatype, B> const& b);

template <data_type::enum_type datatype, typename A, typename B>
bool operator!=(grid_data<datatype, A> const& a,
                grid_data<datatype, B> const& b);

extern template struct grid_data<data_type::fp, std::allocator<void>>;
extern template struct grid_data<data_type::integer, std::allocator<void>>;

extern template bool operator==(
  grid_data<data_type::fp, std::allocator<void>> const& a,
  grid_data<data_type::fp, std::allocator<void>> const& b);

extern template bool operator!=(
  grid_data<data_type::fp, std::allocator<void>> const& a,
  grid_data<data_type::fp, std::allocator<void>> const& b);

extern template bool operator==(
  grid_data<data_type::integer, std::allocator<void>> const& a,
  grid_data<data_type::integer, std::allocator<void>> const& b);

extern template bool operator!=(
  grid_data<data_type::integer, std::allocator<void>> const& a,
  grid_data<data_type::integer, std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/grid_data.ipp>

#endif // XMSH_GRID_DATA_HPP
