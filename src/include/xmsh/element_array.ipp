namespace xmsh {
template <typename A>
element<A>::element(A const& a) : vertex_array(a)
{
}

template <typename A, typename B>
bool operator==(element<A> const& a, element<B> const& b)
{
  bool equality{true};
  equality = equality and (a.index == b.index);
  equality = equality and (a.type == b.type);
  equality = equality and (a.vertex_array == b.vertex_array);
  return equality;
}

template <typename A, typename B>
bool operator!=(element<A> const& a, element<B> const& b)
{
  return (not(a == b));
}

template <typename A, typename B>
bool operator==(element_array<A> const& a, element_array<B> const& b)
{
  bool equality{true};
  equality = equality and a.codim == b.codim;
  if (a.data != b.data) {
    auto adata = a.data;
    auto bdata = b.data;
    auto const compare_func
      = [](auto const& l, auto const& r) { return l.index < r.index; };
    std::sort(std::begin(adata), std::end(adata), compare_func);
    std::sort(std::begin(bdata), std::end(bdata), compare_func);
    equality = equality and (adata == bdata);
  }
  return equality;
}

template <typename A, typename B>
bool operator!=(element_array<A> const& a, element_array<B> const& b)
{
  return (not(a == b));
}

template <typename A>
element_array<A>::element_array(A const& a) : data(a)
{
}

template <typename A>
element_array<A>::element_array(element_array&& ea, A const& a)
: codim(ea.codim), data(std::move(ea.data), a)
{
}

} // namespace xmsh
