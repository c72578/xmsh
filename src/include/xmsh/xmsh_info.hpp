#ifndef XMSH_XMSH_INFO_HPP
#define XMSH_XMSH_INFO_HPP

#include <xmsh/base.hpp>
#include <xmsh/multifile.hpp>
#include <xmsh/version.hpp>

namespace xmsh {
struct xmsh_info {
  Version version{Version::one};
  multifile is_multifile{multifile::no};
};

bool operator==(xmsh_info const& a, xmsh_info const& b);
bool operator!=(xmsh_info const& a, xmsh_info const& b);
} // namespace xmsh
#endif // XMSH_XMSH_INFO_HPP
