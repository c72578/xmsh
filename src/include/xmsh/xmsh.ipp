namespace xmsh {
template <typename A, typename T>
auto read_xmsh(std::string_view const& filename) -> xmsh_data<A>
{
  return xmsh::read_xmsh(filename, A());
}

template <typename A>
auto read_xmsh(std::string_view const& filename, A const& a) -> xmsh_data<A>
{
  return xmsh::read_xmsh(filename, io_type::streamio, a);
}

template <typename A, typename T>
auto read_xmsh(std::string_view const& filename, io_type iotype) -> xmsh_data<A>
{
  return xmsh::read_xmsh(filename, iotype, A());
}

template <typename A>
auto read_xmsh(std::string_view const& filename, io_type iotype, A const& a)
  -> xmsh_data<A>
{
  switch (iotype) {
  case io_type::streamio: {
    return xmsh::detail::streamio::read_xmsh(filename, a);
  }
  default:
    xmsh::detail::bug_log("Unexpected io_type Passed");
    throw std::logic_error("Unexpected io_type Passed");
  }
}

template <typename A>
void write(std::string filename,
           xmsh_info const& xmsh_info,
           xmsh_data<A> const& xmsh)
{
  xmsh::write(filename, xmsh_info, xmsh, io_type::streamio);
}

template <typename A>
void write(std::string filename,
           xmsh_info const& xmsh_info,
           xmsh_data<A> const& xmsh,
           io_type iotype)
{
  switch (iotype) {
  case io_type::streamio: {
    xmsh::detail::streamio::write(filename, xmsh_info, xmsh);
    break;
  }
  default:
    detail::bug_log("Unexpected io_type");
    throw std::logic_error("Unexpected io_type");
  }
}
} // namespace xmsh
