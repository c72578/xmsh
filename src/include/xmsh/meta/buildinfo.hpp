#ifndef XMSH_META_BUILDINFO_HPP
#define XMSH_META_BUILDINFO_HPP

#include <iosfwd>
#include <string_view>
#include <xmsh/meta/project_info.hpp>

namespace xmsh::meta {
#ifdef XMSH_ENABLE_NOEXCEPT
#define XMSH_NOEXCEPT noexcept
#else
#define XMSH_NOEXCEPT
#endif

struct buildinfo_t {
#ifdef DEBUG
  bool debug{true};
#else
  bool debug{false};
#endif // DEBUG

  project_info xmsh_info;
};

std::ostream& operator<<(std::ostream& out, buildinfo_t const& buildinfo);
} // namespace xmsh::meta

#endif // XMSH_META_BUILDINFO_HPP
