#ifndef XMSH_XMSH_HPP
#define XMSH_XMSH_HPP

#include <xmsh/detail/streamio/xmsh.hpp>
#include <xmsh/io_type.hpp>
#include <xmsh/xmsh_data.hpp>
#include <xmsh/xmsh_info.hpp>

namespace xmsh {
template <typename A = std::allocator<void>,
          typename = std::enable_if_t<std::is_default_constructible_v<A>>>
auto read_xmsh(std::string_view const& filename) -> xmsh_data<A>;

template <typename A>
auto read_xmsh(std::string_view const& filename, A const& a) -> xmsh_data<A>;

template <typename A = std::allocator<void>,
          typename = std::enable_if_t<std::is_default_constructible_v<A>>>
auto read_xmsh(std::string_view const& filename, io_type iotype)
  -> xmsh_data<A>;

template <typename A>
auto read_xmsh(std::string_view const& filename, io_type iotype, A const& a)
  -> xmsh_data<A>;

template <typename A>
void write(std::string filename,
           xmsh_info const& xmsh_info,
           xmsh_data<A> const& xmsh);

template <typename A>
void write(std::string filename,
           xmsh_info const& xmsh_info,
           xmsh_data<A> const& xmsh,
           io_type iotype);

extern template auto read_xmsh(std::string_view const& filename)
  -> xmsh_data<std::allocator<void>>;

extern template auto read_xmsh(std::string_view const& filename,
                               std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

extern template auto read_xmsh(std::string_view const& filename, io_type iotype)
  -> xmsh_data<std::allocator<void>>;

extern template auto read_xmsh(std::string_view const& filename,
                               io_type iotype,
                               std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

extern template void write(std::string filename,
                           xmsh_info const& xmsh_info,
                           xmsh_data<std::allocator<void>> const& xmsh);

extern template void write(std::string filename,
                           xmsh_info const& xmsh_info,
                           xmsh_data<std::allocator<void>> const& xmsh,
                           io_type iotype);
} // namespace xmsh

#include <xmsh/xmsh.ipp>

#endif // XMSH_XMSH_HPP
