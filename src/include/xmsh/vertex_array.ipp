namespace xmsh {
template <typename A>
Vertex<A>::Vertex(A const& a) : coord(a)
{
}

template <typename A>
Vertex<A>::Vertex(Vertex&& vertex, A const& a)
: index(vertex.index), coord(std::move(vertex.coord), a)
{
}

template <typename A, typename B>
bool operator==(Vertex<A> const& a, Vertex<B> const& b)
{
  bool equality{true};
  constexpr auto const epsilon = std::numeric_limits<double>::epsilon();
  equality = equality and (a.index == b.index);
  equality = equality and (a.coord.size() == b.coord.size());
  for (std::size_t i = 0; i < a.coord.size(); ++i) {
    equality = equality and (std::abs(a.coord[i] - b.coord[i]) < epsilon);
  }
  return equality;
}

template <typename A, typename B>
bool operator!=(Vertex<A> const& a, Vertex<B> const& b)
{
  return (not(a == b));
}

template <typename A>
vertex_array<A>::vertex_array(A const& a) : data(a)
{
}

template <typename A>
vertex_array<A>::vertex_array(vertex_array&& vertex_array, A const& a)
: data(std::move(vertex_array.data), a)
{
}

template <typename A, typename B>
bool operator==(vertex_array<A> const& a, vertex_array<B> const& b)
{
  bool equality{true};
  if (a.data != b.data) {
    auto adata = a.data;
    auto bdata = b.data;
    auto const compare_func
      = [](auto const& l, auto const& r) { return l.index < r.index; };
    std::sort(std::begin(adata), std::end(adata), compare_func);
    std::sort(std::begin(bdata), std::end(bdata), compare_func);
    equality = equality and (adata == bdata);
  }
  return equality;
}

template <typename A, typename B>
bool operator!=(vertex_array<A> const& a, vertex_array<B> const& b)
{
  return (not(a == b));
}

} // namespace xmsh
