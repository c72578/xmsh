#ifndef XMSH_ENUMERATION_HPP
#define XMSH_ENUMERATION_HPP

#include <algorithm>
#include <cstring>
#include <optional>
#include <string_view>
#include <type_traits>
#include <xmsh/base.hpp>
#include <xmsh/detail/log.hpp>

namespace xmsh {
template <typename Enumeration>
auto to_enumeration(typename Enumeration::underlying_type const& uv)
  -> Enumeration
{
  static_assert(is_enumeration<Enumeration>::value);
  constexpr auto const& option_array = Enumeration::option_array;
  auto const it
    = std::find_if(std::begin(option_array),
                   std::end(option_array),
                   [uv](auto const& a) { return a.underlying_value() == uv; });
  if (it != std::end(option_array)) {
    return *it;
  }
  throw std::runtime_error("Unexpected underlying_value encountered.");
}

template <typename Enumeration>
auto to_enumeration(typename Enumeration::enum_type const& e) -> Enumeration
{
  static_assert(is_enumeration<Enumeration>::value);
  constexpr auto const& option_array = Enumeration::option_array;
  auto const it = std::find_if(std::begin(option_array),
                               std::end(option_array),
                               [e](auto const& a) { return a == e; });
  if (it != std::end(option_array)) {
    return *it;
  }
  throw std::runtime_error("Unexpected enum value encountered.");
}

template <typename Enumeration>
auto to_enumeration(std::string_view input) -> Enumeration
{
  static_assert(is_enumeration<Enumeration>::value);
  constexpr auto const& option_string_array = Enumeration::option_string_array;

  auto const it = std::find(
    std::begin(option_string_array), std::end(option_string_array), input);

  if (it != std::end(option_string_array)) {
    auto const index
      = static_cast<std::size_t>(it - std::begin(option_string_array));
    return Enumeration::option_array[index];
  }
  throw std::runtime_error("Unexpected string representation encountered.");
}

template <typename Enumeration,
          typename = std::enable_if_t<is_enumeration<Enumeration>::value>>
auto to_string_view(Enumeration const& enumeration) -> std::string_view
{
  constexpr auto const& option_array = Enumeration::option_array;
  constexpr auto const& option_string_array = Enumeration::option_string_array;

  auto const it
    = std::find(std::begin(option_array), std::end(option_array), enumeration);

  auto const index = static_cast<std::size_t>(it - std::begin(option_array));

  return option_string_array[index];
}

template <typename Enumeration,
          typename = std::enable_if_t<is_enumeration<Enumeration>::value>>
auto to_qualified_string_view(Enumeration const& enumeration)
  -> std::string_view
{
  constexpr auto const& option_array = Enumeration::option_array;
  constexpr auto const& option_string_array = Enumeration::option_string_array;

  auto const it
    = std::find(std::begin(option_array), std::end(option_array), enumeration);

  auto const index = static_cast<std::size_t>(it - std::begin(option_array));

  return std::string(Enumeration::namespace_string)
         + std::string(Enumeration::string)
         + "::" + std::string(option_string_array[index]);
}

template <typename Enumeration,
          typename A = std::allocator<void>,
          typename = std::enable_if_t<is_enumeration<Enumeration>::value>>
auto read_key_value_pair(std::istream& in, A const& a = A()) -> Enumeration
{

  string<A> input(a);
  in >> input;
  if (input != Enumeration::string) {
    xmsh::detail::debug_log("Expected " + string<A>(Enumeration::string)
                            + ". Found " + input + " instead.");
    throw std::runtime_error("Unexpected Key Found");
  }

  in >> input;
  return xmsh::to_enumeration<Enumeration>(std::stoul(input));
}

template <typename Enumeration,
          typename A = std::allocator<void>,
          typename = std::enable_if_t<is_enumeration<Enumeration>::value>>
auto read_key_string_pair(std::istream& in, A const& a = A()) -> Enumeration
{
  string<A> input(a);
  in >> input;
  if (input != Enumeration::string) {
    detail::debug_log("Expected " + string<A>(Enumeration::string) + ". Found "
                      + input + " instead.");
    throw std::runtime_error("Unexpected Key Found");
  }

  in >> input;
  return xmsh::to_enumeration<Enumeration>(input.c_str());
}

template <typename Enumeration,
          typename = std::enable_if_t<is_enumeration<Enumeration>::value>>
auto write_key_string_pair(std::ostream& out, Enumeration const& e)
{
  out << Enumeration::string << " " << to_string_view(e) << "\n";
}

template <typename Enumeration,
          typename = std::enable_if_t<is_enumeration<Enumeration>::value>>
auto write_key_value_pair(std::ostream& out, Enumeration const& e)
{
  out << Enumeration::string << " " << e.underlying_value() << "\n";
}
} // namespace xmsh

#endif // XMSH_ENUMERATION_HPP
