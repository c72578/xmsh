namespace xmsh {
template <typename A>
grid_info<A>::grid_info(A const& a) : label(a)
{
}

template <typename A>
grid_info<A>::grid_info(grid_info&& fi, A const& a)
: label(std::move(fi.label), a), wdim(fi.wdim), dim(fi.dim)
{
}

template <typename A>
grid_info<A>::grid_info(grid_info const& fi, A const& a)
: label(fi.label, a), wdim(fi.wdim), dim(fi.dim)
{
}

template <typename A, typename B>
bool operator==(grid_info<A> const& a, grid_info<B> const& b)
{
  bool equality{true};
  equality = equality and (a.label == b.label);
  equality = equality and (a.wdim == b.wdim);
  equality = equality and (a.dim == b.dim);
  return equality;
}

template <typename A, typename B>
bool operator!=(grid_info<A> const& a, grid_info<B> const& b)
{
  return (not(a == b));
}
} // namespace xmsh
