#ifndef XMSH_GRID_INFO_HPP
#define XMSH_GRID_INFO_HPP

#include <xmsh/base.hpp>
#include <xmsh/grid_dimension.hpp>
#include <xmsh/world_dimension.hpp>

namespace xmsh {
template <typename A = std::allocator<void>>
struct grid_info {
  using allocator_type = A;

  string<A> label;
  world_dimension wdim{world_dimension::one};
  grid_dimension dim{grid_dimension::one};

  grid_info() = default;
  explicit grid_info(A const& a);

  grid_info(grid_info const& fi) = default;
  grid_info& operator=(grid_info const& fi) = default;
  grid_info& operator=(grid_info&& fi) noexcept = default;
  grid_info(grid_info&& fi) noexcept = default;

  grid_info(grid_info const& fi, A const& a);
  grid_info(grid_info&& fi, A const& a);

  ~grid_info() = default;
};

template <typename A, typename B>
bool operator==(grid_info<A> const& a, grid_info<B> const& b);
template <typename A, typename B>
bool operator!=(grid_info<A> const& a, grid_info<B> const& b);

extern template struct grid_info<std::allocator<void>>;
extern template bool operator==(grid_info<std::allocator<void>> const& a,
                                grid_info<std::allocator<void>> const& b);
extern template bool operator!=(grid_info<std::allocator<void>> const& a,
                                grid_info<std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/grid_info.ipp>

#endif // XMSH_GRID_INFO_HPP
