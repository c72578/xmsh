#ifndef XMSH_FILE_ARRAY_HPP
#define XMSH_FILE_ARRAY_HPP

#include <xmsh/base.hpp>
#include <xmsh/file_type.hpp>

namespace xmsh {
template <typename A = std::allocator<void>>
struct file_info {
  using allocator_type = A;

  std::size_t index{0};
  string<A> filename;
  struct file_type file_type{file_type::ascii};

  file_info() = default;
  explicit file_info(A const& a);

  file_info(file_info const& fi) = default;
  file_info& operator=(file_info const& fi) = default;
  file_info(file_info&& fi) noexcept = default;
  file_info& operator=(file_info&& fi) noexcept = default;

  file_info(file_info const& fi, A const& a);
  file_info(file_info&& fi, A const& a);
  ~file_info() = default;
};

template <typename A, typename B>
bool operator==(file_info<A> const& a, file_info<B> const& b);
template <typename A, typename B>
bool operator!=(file_info<A> const& a, file_info<B> const& b);

extern template struct file_info<std::allocator<void>>;
extern template bool operator==(file_info<std::allocator<void>> const& a,
                                file_info<std::allocator<void>> const& b);
extern template bool operator!=(file_info<std::allocator<void>> const& a,
                                file_info<std::allocator<void>> const& b);

template <typename A = std::allocator<void>>
struct file_array {
  using allocator_type = A;

  vector<file_info<A>, A> data;

  file_array() = default;
  explicit file_array(A const& a);

  file_array(file_array const& fia) = default;
  file_array& operator=(file_array const& fia) = default;
  file_array(file_array&& fia) noexcept = default;
  file_array& operator=(file_array&& fia) noexcept = default;

  file_array(file_array const& fia, A const& a);
  file_array(file_array&& fia, A const& a);
  ~file_array() = default;
};

template <typename A, typename B>
bool operator==(file_array<A> const& a, file_array<B> const& b);
template <typename A, typename B>
bool operator!=(file_array<A> const& a, file_array<B> const& b);

extern template struct file_array<std::allocator<void>>;
extern template bool operator==(file_array<std::allocator<void>> const& a,
                                file_array<std::allocator<void>> const& b);
extern template bool operator!=(file_array<std::allocator<void>> const& a,
                                file_array<std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/file_array.ipp>

#endif // XMSH_FILE_ARRAY_HPP
