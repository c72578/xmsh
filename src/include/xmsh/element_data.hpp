#ifndef XMSH_ELEMENT_DATA_HPP
#define XMSH_ELEMENT_DATA_HPP

#include <cstdint>
#include <iostream>
#include <xmsh/data_type.hpp>

namespace xmsh {
template <data_type::enum_type datatype>
struct Scalar;

template <>
struct Scalar<data_type::fp> {
  using type = double;
};

template <>
struct Scalar<data_type::integer> {
  using type = std::int64_t;
};

template <data_type::enum_type datatype, typename A = std::allocator<void>>
struct element_data {
  using allocator_type = A;

  using ScalarType = typename Scalar<datatype>::type;
  element_data() = default;
  explicit element_data(A const& a);

  std::size_t index{detail::max_size};
  vector<ScalarType, A> value;
};

template <data_type::enum_type datatype, typename A, typename B>
bool operator==(element_data<datatype, A> const& a,
                element_data<datatype, B> const& b);

template <data_type::enum_type datatype, typename A, typename B>
bool operator!=(element_data<datatype, A> const& a,
                element_data<datatype, B> const& b);

extern template struct element_data<data_type::fp, std::allocator<void>>;
extern template struct element_data<data_type::integer, std::allocator<void>>;

extern template bool operator==(
  element_data<data_type::fp, std::allocator<void>> const& a,
  element_data<data_type::fp, std::allocator<void>> const& b);

extern template bool operator!=(
  element_data<data_type::fp, std::allocator<void>> const& a,
  element_data<data_type::fp, std::allocator<void>> const& b);

extern template bool operator==(
  element_data<data_type::integer, std::allocator<void>> const& a,
  element_data<data_type::integer, std::allocator<void>> const& b);

extern template bool operator!=(
  element_data<data_type::integer, std::allocator<void>> const& a,
  element_data<data_type::integer, std::allocator<void>> const& b);

} // namespace xmsh

#include <xmsh/element_data.ipp>

#endif // XMSH_ELEMENT_DATA_HPP
