namespace xmsh {
template <typename A>
xmsh_data<A>::xmsh_data(A const& a)
: grid_info(a),
  vertex_array(a),
  codim_element_array(a),
  float_grid_data(a),
  integer_grid_data(a)
{
}

template <typename A>
xmsh_data<A>::xmsh_data(xmsh_data&& xmsh, A const& a)
: grid_info(std::move(xmsh.grid_info), a),
  vertex_array(std::move(xmsh.vertex_array), a),
  codim_element_array(std::move(xmsh.codim_element_array), a),
  float_grid_data(std::move(xmsh.float_grid_data), a),
  integer_grid_data(std::move(xmsh.integer_grid_data), a)
{
}

template <typename A, typename B>
bool operator==(xmsh_data<A> const& a, xmsh_data<B> const& b)
{
  bool equality{true};
  equality = equality and (a.grid_info == b.grid_info);
  equality = equality and (a.vertex_array == b.vertex_array);
  equality = equality and (a.codim_element_array == b.codim_element_array);
  equality = equality and (a.float_grid_data == b.float_grid_data);
  equality = equality and (a.integer_grid_data == b.integer_grid_data);
  return equality;
}

template <typename A, typename B>
bool operator!=(xmsh_data<A> const& a, xmsh_data<B> const& b)
{
  return (not(a == b));
}

} // namespace xmsh
