namespace xmsh {
template <data_type::enum_type datatype, typename A>
grid_data<datatype, A>::grid_data(A const& a) : label(a), data(a)
{
}

template <data_type::enum_type datatype, typename A>
grid_data<datatype, A>::grid_data(grid_data&& dl, A const& a)
: label(std::move(dl.label)), data(std::move(dl.data), a)
{
}

template <data_type::enum_type datatype, typename A, typename B>
bool operator==(grid_data<datatype, A> const& a, grid_data<datatype, B> const& b)
{
  bool equality{true};
  equality = equality and (a.codim == b.codim);
  equality = equality and (a.label == b.label);
  if (a.data != b.data) {
    auto adata = a.data;
    auto bdata = b.data;
    auto const compare_func
      = [](auto const& l, auto const& r) { return l.index < r.index; };
    std::sort(std::begin(adata), std::end(adata), compare_func);
    std::sort(std::begin(bdata), std::end(bdata), compare_func);
    equality = equality and (adata == bdata);
  }

  return equality;
}

template <data_type::enum_type datatype, typename A, typename B>
bool operator!=(grid_data<datatype, A> const& a, grid_data<datatype, B> const& b)
{
  return (not(a == b));
}
} // namespace xmsh
