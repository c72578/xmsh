#include <iostream>
#include <xmsh/meta/buildinfo.hpp>

namespace xmsh::meta {
std::ostream& operator<<(std::ostream& out, buildinfo_t const& buildinfo)
{
  out << "{\n";
  out << "  \"version\": {\n";
  out << "    \"major\" :" << buildinfo.xmsh_info.major_version << ",\n";
  out << "    \"minor\" :" << buildinfo.xmsh_info.minor_version << ",\n";
  out << "    \"patch\" :" << buildinfo.xmsh_info.patch_version << ",\n";
  out << "  }\n";
  out << "}\n";
  return out;
}
} // namespace xmsh::meta
