#include <xmsh/meta/project_info.hpp>

namespace xmsh::meta {
std::ostream& operator<<(std::ostream& out, project_info const& projectinfo)
{
  out << projectinfo.major_version << "." << projectinfo.minor_version << "."
      << projectinfo.patch_version;
  return out;
}
} // namespace xmsh::meta
