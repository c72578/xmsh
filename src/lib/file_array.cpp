#include <xmsh/file_array.hpp>

namespace xmsh {
template struct file_info<std::allocator<void>>;
template bool operator==(file_info<std::allocator<void>> const& a,
                         file_info<std::allocator<void>> const& b);
template bool operator!=(file_info<std::allocator<void>> const& a,
                         file_info<std::allocator<void>> const& b);

template struct file_array<std::allocator<void>>;
template bool operator==(file_array<std::allocator<void>> const& a,
                         file_array<std::allocator<void>> const& b);
template bool operator!=(file_array<std::allocator<void>> const& a,
                         file_array<std::allocator<void>> const& b);

} // namespace xmsh
