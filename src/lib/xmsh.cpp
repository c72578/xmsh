#include <xmsh/xmsh.hpp>

namespace xmsh {
template auto read_xmsh(std::string_view const& filename)
  -> xmsh_data<std::allocator<void>>;

template auto read_xmsh(std::string_view const& filename,
                        std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

template auto read_xmsh(std::string_view const& filename, io_type iotype)
  -> xmsh_data<std::allocator<void>>;

template auto read_xmsh(std::string_view const& filename,
                        io_type iotype,
                        std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

template void write(std::string filename,
                    xmsh_info const& xmsh_info,
                    xmsh_data<std::allocator<void>> const& xmsh);

template void write(std::string filename,
                    xmsh_info const& xmsh_info,
                    xmsh_data<std::allocator<void>> const& xmsh,
                    io_type iotype);

} // namespace xmsh
