#include <xmsh/xmsh_info.hpp>

namespace xmsh {
bool operator==(xmsh_info const& a, xmsh_info const& b)
{
  bool equality{true};
  equality = equality and (a.version == b.version);
  equality = equality and (a.is_multifile == b.is_multifile);
  return equality;
}

bool operator!=(xmsh_info const& a, xmsh_info const& b)
{
  return (not(a == b));
}
} // namespace xmsh
