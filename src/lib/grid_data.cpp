#include <xmsh/grid_data.hpp>

namespace xmsh {
template struct grid_data<data_type::fp, std::allocator<void>>;
template struct grid_data<data_type::integer, std::allocator<void>>;

template bool operator==(
  grid_data<data_type::fp, std::allocator<void>> const& a,
  grid_data<data_type::fp, std::allocator<void>> const& b);

template bool operator!=(
  grid_data<data_type::fp, std::allocator<void>> const& a,
  grid_data<data_type::fp, std::allocator<void>> const& b);

template bool operator==(
  grid_data<data_type::integer, std::allocator<void>> const& a,
  grid_data<data_type::integer, std::allocator<void>> const& b);

template bool operator!=(
  grid_data<data_type::integer, std::allocator<void>> const& a,
  grid_data<data_type::integer, std::allocator<void>> const& b);
} // namespace xmsh
