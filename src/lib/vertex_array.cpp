#include <xmsh/vertex_array.hpp>

namespace xmsh {
template struct Vertex<std::allocator<void>>;
template bool operator==(Vertex<std::allocator<void>> const& a,
                         Vertex<std::allocator<void>> const& b);
template bool operator!=(Vertex<std::allocator<void>> const& a,
                         Vertex<std::allocator<void>> const& b);

template struct vertex_array<std::allocator<void>>;
template bool operator==(vertex_array<std::allocator<void>> const& a,
                         vertex_array<std::allocator<void>> const& b);
template bool operator!=(vertex_array<std::allocator<void>> const& a,
                         vertex_array<std::allocator<void>> const& b);

} // namespace xmsh
