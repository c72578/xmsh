#include <xmsh/element_array.hpp>

namespace xmsh {
template struct element<std::allocator<void>>;
template bool operator==(element<std::allocator<void>> const& a,
                         element<std::allocator<void>> const& b);
template bool operator!=(element<std::allocator<void>> const& a,
                         element<std::allocator<void>> const& b);

template struct element_array<std::allocator<void>>;
template bool operator==(element_array<std::allocator<void>> const& a,
                         element_array<std::allocator<void>> const& b);
template bool operator!=(element_array<std::allocator<void>> const& a,
                         element_array<std::allocator<void>> const& b);
} // namespace xmsh
