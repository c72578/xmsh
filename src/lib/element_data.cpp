#include <cmath>
#include <xmsh/element_data.hpp>

namespace xmsh {
template struct element_data<data_type::fp, std::allocator<void>>;
template struct element_data<data_type::integer, std::allocator<void>>;

template bool operator==(
  element_data<data_type::fp, std::allocator<void>> const& a,
  element_data<data_type::fp, std::allocator<void>> const& b);

template bool operator!=(
  element_data<data_type::fp, std::allocator<void>> const& a,
  element_data<data_type::fp, std::allocator<void>> const& b);

template bool operator==(
  element_data<data_type::integer, std::allocator<void>> const& a,
  element_data<data_type::integer, std::allocator<void>> const& b);

template bool operator!=(
  element_data<data_type::integer, std::allocator<void>> const& a,
  element_data<data_type::integer, std::allocator<void>> const& b);
} // namespace xmsh
