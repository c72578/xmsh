#include <xmsh/xmsh_data.hpp>

namespace xmsh {
template struct xmsh_data<std::allocator<void>>;
template bool operator==(xmsh_data<std::allocator<void>> const& a,
                         xmsh_data<std::allocator<void>> const& b);
template bool operator!=(xmsh_data<std::allocator<void>> const& a,
                         xmsh_data<std::allocator<void>> const& b);
} // namespace xmsh
