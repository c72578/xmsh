#include <xmsh/grid_info.hpp>

namespace xmsh {
template struct grid_info<std::allocator<void>>;
template bool operator==(grid_info<std::allocator<void>> const& a,
                         grid_info<std::allocator<void>> const& b);
template bool operator!=(grid_info<std::allocator<void>> const& a,
                         grid_info<std::allocator<void>> const& b);
} // namespace xmsh
