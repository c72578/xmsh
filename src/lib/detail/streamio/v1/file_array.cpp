#include <xmsh/detail/streamio/v1/file_array.hpp>

namespace xmsh::detail::streamio::v1 {
template auto read_file_array(std::istream& in, std::allocator<void> const& a)
  -> file_array<std::allocator<void>>;

template void write(std::ostream& out,
                    file_array<std::allocator<void>> const& filearray);
} // namespace xmsh::detail::streamio::v1
