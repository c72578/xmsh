#include <xmsh/detail/streamio/v1/element_array.hpp>

namespace xmsh::detail::streamio::v1 {
template auto read_element_array(std::istream& in,
                                 std::allocator<void> const& a)
  -> element_array<std::allocator<void>>;

template void write(std::ostream& out,
                    element_array<std::allocator<void>> const& element_array);
} // namespace xmsh::detail::streamio::v1
