#include <xmsh/detail/streamio/v1/grid_info.hpp>

namespace xmsh::detail::streamio::v1 {
template auto read_grid_info(std::istream& in, std::allocator<void> const& a)
  -> grid_info<std::allocator<void>>;

template void write(std::ostream& out,
                    grid_info<std::allocator<void>> const& grid_info);
} // namespace xmsh::detail::streamio::v1
