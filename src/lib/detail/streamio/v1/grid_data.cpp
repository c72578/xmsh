#include <xmsh/detail/streamio/v1/grid_data.hpp>

namespace xmsh::detail::streamio::v1 {
template auto read_grid_data(std::istream& in, std::allocator<void> const& a)
  -> grid_data<data_type::fp, std::allocator<void>>;

template void write(
  std::ostream& out,
  grid_data<data_type::fp, std::allocator<void>> const& grid_data);

template auto read_grid_data(std::istream& in, std::allocator<void> const& a)
  -> grid_data<data_type::integer, std::allocator<void>>;

template void write(
  std::ostream& out,
  grid_data<data_type::integer, std::allocator<void>> const& grid_data);

} // namespace xmsh::detail::streamio::v1
