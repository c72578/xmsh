#include <xmsh/detail/streamio/v1/to_section_type.hpp>

namespace xmsh::detail::streamio::v1 {
auto header_to_section_type(std::string_view const& header) -> section_type
{
  if (header == SectionTraits<section_type::vertexarray>::header) {
    return section_type::vertexarray;
  }
  if (header == SectionTraits<section_type::elementarray>::header) {
    return section_type::elementarray;
  }
  if (header == SectionTraits<section_type::griddata>::header) {
    return section_type::griddata;
  }
  throw std::runtime_error("Unexpected Header for Section Type");
}
} // namespace xmsh::detail::streamio::v1
