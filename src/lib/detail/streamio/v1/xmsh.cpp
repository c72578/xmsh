#include <xmsh/detail/streamio/v1/xmsh.hpp>

namespace xmsh::detail::streamio::v1 {
template struct SingleFileReadStatus<std::allocator<void>>;

template auto read_singlefile_xmsh(std::istream& in,
                                   std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

template void write_singlefile_xmsh(
  std::ostream& out, xmsh_data<std::allocator<void>> const& xmsh);

template auto read_xmsh(std::istream& in,
                        xmsh_info const& xmsh_info,
                        std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

template void write(std::ostream& filename,
                    xmsh_info const& xmsh_info,
                    xmsh_data<std::allocator<void>> const& xmsh);
} // namespace xmsh::detail::streamio::v1
