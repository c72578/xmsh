#include <xmsh/detail/streamio/v1/vertex_array.hpp>

namespace xmsh::detail::streamio::v1 {
template auto read_vertex_array(std::istream& in, std::allocator<void> const& a)
  -> vertex_array<std::allocator<void>>;

template void write(std::ostream& out,
                    vertex_array<std::allocator<void>> const& vertex_array);

} // namespace xmsh::detail::streamio::v1
