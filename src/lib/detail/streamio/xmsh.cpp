#include <xmsh/detail/streamio/xmsh.hpp>
namespace xmsh::detail::streamio {
template auto read_xmsh(std::string_view const& filename,
                        std::allocator<void> const& a)
  -> xmsh_data<std::allocator<void>>;

template void write(std::string_view const& filename,
                    xmsh_info xmsh_info,
                    xmsh_data<std::allocator<void>> const& xmsh);

} // namespace xmsh::detail::streamio
