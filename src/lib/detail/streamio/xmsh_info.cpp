#include <xmsh/detail/streamio/xmsh_info.hpp>

namespace xmsh::detail::streamio {
template auto read_xmsh_info<std::allocator<void>>(std::istream& in)
  -> xmsh_info;

template auto read_xmsh_info(std::istream& in, std::allocator<void> const& a)
  -> xmsh_info;

void write(std::ostream& out, xmsh_info const& xmsh_info)
{
  out << "$xmshinfo"
      << "\n";
  xmsh::write_key_value_pair(out, xmsh_info.version);
  xmsh::write_key_string_pair(out, xmsh_info.is_multifile);
  out << "$endxmshinfo"
      << "\n";
}
} // namespace xmsh::detail::streamio
