#include <xmsh/detail/log.hpp>
#include <xmsh/meta/buildinfo.hpp>

namespace xmsh::detail {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"
void debug_log(std::string_view message)
{
  constexpr auto buildinfo = meta::buildinfo_t();
  if constexpr (buildinfo.debug) {
    std::cout << message;
  }
}
#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"
void bug_log(std::string_view message)
{
  constexpr auto buildinfo = meta::buildinfo_t();
  if constexpr (buildinfo.debug) {
    std::cerr << "[XMSH] FATAL : " << message << "\n";
    std::cerr << "You've encountered a bug in xmsh.\n"
              << "You can help us fix it by reporting it to "
              << buildinfo.xmsh_info.url << ".\n"; // NOLINT
    // TODO(jayesh): Enable the following code when you can get
    // `boost::backtrace` working with cmake configuration.
    //            << "Please include the following backtrace in your report.\n"
    //            << boost::stacktrace::stacktrace();
  }
}
#pragma clang diagnostic pop
} // namespace xmsh::detail
