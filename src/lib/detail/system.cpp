#include <xmsh/detail/log.hpp>
#include <xmsh/detail/system.hpp>

namespace xmsh::detail {
bool validate(std::string_view s1, std::string_view s2)
{
  if (s1 == s2) {
    return true;
  }
  detail::debug_log("Expected ");
  detail::debug_log(s2);
  detail::debug_log(". Found ");
  detail::debug_log(s1);
  detail::debug_log(" instead.\n");
  return false;
}
} // namespace xmsh::detail
