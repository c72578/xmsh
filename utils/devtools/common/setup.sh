#!/bin/bash
find *  -type d -name "devconfig" -prune -o  -print -type d -name "tex" -prune -o -print -exec sed -i "s/xmsh/$1/g" {} \;
find *  -type d -name "devconfig" -prune -o  -print -type d -name "tex" -prune -o -print -exec sed -i "s/XMSH/$2/g" {} \;

if [ -d src/include/xmsh ]; then
  mv src/include/xmsh src/include/$1
  mv src/lib/include/xmsh src/lib/include/$1
  mv src/enum/xmsh src/enum/$1
  mv test/enum/testxmsh test/enum/test$1
  mkdir test/include/
fi
